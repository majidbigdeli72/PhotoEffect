#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "opencv/highgui.h"
#include "stasm/stasm_lib.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include <Windows.h>
#include "photoShopBlends.h" 
#define makeFaceBox(landmarks) cv::Rect(landmarks[0],landmarks[14*2+1],landmarks[11*2]-landmarks[0],landmarks[6*2+1]-landmarks[14*2+1]);

using namespace cv;
using namespace std;

struct flagSpec{
	double faceRatio,foreHeadRatio;
	float xFaceOffset, yFaceOffset;
	float xForeHeadOffset, yForeHeadOffset;
	string foreHeadFilePath, faceImgFilePath;
	Mat foreHeadImg,faceImg;
};
class flagger
{
private:
	void loadFlags(string XMLFilePath, flagSpec *flagData);
	//void addFlag3(Mat *img, cv::Rect flagBox, int flagNumber);
	void addFaceFlag(Mat *img, cv::Rect flagBox, int faceNumber,int lr);
	//void addFlag(Mat *img, cv::Rect faceBox, int hatNumber);
	void addForeHeadFlag(Mat *img, cv::Rect flagBox, int foreHeadNumber);

	vector<cv::Point> ROI_Poly;
	vector<cv::Point> ROI_Vertices;

	vector<flagSpec*> flags;
	//Mat maskFace,maskEyes,maskTotal;
	Mat faceEffectMask, img;
	Mat foreHeadEffectMask;
	cv::Rect faceRect;
	Mat flagI, flagDist;
	std::string workingDir;
	//Mat hatDist;
public:
	int test(Mat imgscr, Mat *imgdes, int flagNumber);
	flagger(string workingPath, int flgNumbers);
	~flagger(void);
};

