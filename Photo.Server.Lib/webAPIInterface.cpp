#include "stdafx.h"
#include "webAPIInterface.h"
using namespace PhotoServerLib;


void webAPIInterface::MarshalString(System::String ^ s, string& os) {
	using namespace Runtime::InteropServices;
	const char* chars =
		(const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
	os = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
}
/*
void MarshalString(System::String ^ s, wstring& os) {
	using namespace Runtime::InteropServices;
	const wchar_t* chars =
		(const wchar_t*)(Marshal::StringToHGlobalUni(s)).ToPointer();
	os = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
}
*/
webAPIInterface::webAPIInterface(System::String ^workingPath){

	std::string pathStd;
	MarshalString(workingPath, pathStd);
	flaggerEngine = new flagger(pathStd, 3);
	myTextAdder = gcnew textAdder(workingPath,18);
	hajiMakerEngine = new hajiFirooz(pathStd, 6);
	MatBitmapWrapper ^mat2Bmp = gcnew MatBitmapWrapper(pathStd);
	watermarkDrawFont = gcnew System::Drawing::Font("Times", 20);
	watermarkDrawBrush= gcnew SolidBrush(Color::Red);
	watermarkBackDrawBrush = gcnew SolidBrush(Color::White);
	timeBuf = new char[100];
	workingDir = new std::string(pathStd);


}

Bitmap ^ webAPIInterface::cartoofify(Bitmap ^inp, float level){
	cv::Mat img = mat2Bmp->Bmp2Mat(inp);
	cv::Mat imout = img.clone();


	cv::stylization(img, imout, 60, level);
	Bitmap ^res=  mat2Bmp->Mat2Bmp(imout);

	imout.release();
	img.release();
	return res;


}

Bitmap ^ webAPIInterface::flagIt(Bitmap ^inp, float level){
	cv::Mat img = mat2Bmp->Bmp2Mat(inp);
	cv::Mat imout = img.clone();


	flaggerEngine->test(img, &imout, (int)level);
	Bitmap ^res = mat2Bmp->Mat2Bmp(imout);

	imout.release();
	img.release();
	return res;
}
/*
Bitmap ^ webAPIInterface::addText(System::String ^text, Bitmap ^bitmap){
	return bitmap;
}
*/
Bitmap ^ webAPIInterface::addText(System::String ^signature, System::String ^text, int postalCardNumber){
	try{
		if (text == "1")
		{
			return waterMarkParmik(myTextAdder->addText(signature, postalCardNumber - 1));

		}
		else if (text == "2"){
			return waterMarkParmik(myTextAdder->addText(signature, postalCardNumber + 6 * 1 - 1));
		}
		else {
			return waterMarkParmik(myTextAdder->addText(signature, text, postalCardNumber + 6 * 2 - 1));
		}
	}
	catch (std::exception &e){
		std::ofstream ofp;
		time_t now = time(0);
		struct tm  tstruct;
		tstruct = *localtime(&now);

		strftime(timeBuf, sizeof(timeBuf), "%Y-%m-%d.%X", &tstruct);

		ofp.open(*workingDir + "/webAPIInterfce.log", std::ofstream::app);
		ofp << timeBuf << " textAdder: " << e.what() << endl;
		ofp.close();
	}
	
}
Bitmap ^ webAPIInterface::waterMarkParmik(Bitmap ^inp){
	try{
		//System::String ^drawString = gcnew System::String("@parmik");

		// Create font and brush.
		
		if (inp->Height > 50 && inp->Width > 240)
		{


			// Create point for upper-left corner of drawing.
			PointF drawPoint(10.0F, inp->Height - 40.0F);
			System::Drawing::Graphics ^graphics = System::Drawing::Graphics::FromImage(inp);
			graphics->FillRectangle(watermarkBackDrawBrush, System::Drawing::Rectangle(9.0f, inp->Height - 38.0F, 220, 32));
			// Draw string to screen.
			graphics->DrawString("@ParmikPhotoBot", watermarkDrawFont, watermarkDrawBrush, drawPoint);
		}
		return inp;
	}
	catch (std::exception &e){
		std::ofstream ofp;
		time_t now = time(0);
		struct tm  tstruct;
		tstruct = *localtime(&now);

		strftime(timeBuf, sizeof(timeBuf), "%Y-%m-%d.%X", &tstruct);

		ofp.open(*workingDir + "/webAPIInterfce.log", std::ofstream::app);
		ofp << timeBuf << " watermarker: " << e.what() << endl;
		ofp.close();
	}

}

Bitmap ^ webAPIInterface::hajiMaker(Bitmap ^ bmp, int hatNumber, int darknessType){
	try{


		cv::Mat img = mat2Bmp->Bmp2Mat(bmp);
		if (img.cols > 2000 | img.rows > 2000){
			if (img.cols > img.rows)
			{
				double ratio = (double)2000.f / (double)img.cols;
				resize(img, img, cv::Size(), ratio, ratio);
			}
			else {

				double ratio = (double)2000.f / (double)img.rows;
				resize(img, img, cv::Size(), ratio, ratio);

			}

		}
		cv::Mat imout = img.clone();


		hajiMakerEngine->makeFirooz(img, &imout, hatNumber, darknessType);
		Bitmap ^res = waterMarkParmik(mat2Bmp->Mat2Bmp((imout)));

		imout.release();
		img.release();
		return res;
	}
	catch (std::exception &e){
		std::ofstream ofp;
		time_t now = time(0);
		struct tm  tstruct;
		tstruct = *localtime(&now);

		strftime(timeBuf, sizeof(timeBuf), "%Y-%m-%d.%X", &tstruct);

		ofp.open(*workingDir + "/webAPIInterfce.log", std::ofstream::app);
		ofp << timeBuf << " hajiMakerInterface: " << e.what() << endl;
		ofp.close();

	}
}
