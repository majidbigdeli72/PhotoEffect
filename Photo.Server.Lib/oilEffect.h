#pragma once
#include <opencv2\opencv.hpp>
using namespace cv;
using namespace std;
class oilEffect
{
private:

public:
	bool doPont(Mat src, Mat dst);
	void Process(Mat *pbyDataIn_i,
		const int nRadius_i,
		const float fIntensityLevels_i,
		Mat* pbyDataOut_o);
	oilEffect();
	~oilEffect();
};

