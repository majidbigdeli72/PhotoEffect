#include "StdAfx.h"
#include "textAdder.h"

using namespace PhotoServerLib;

void textAdder::MarshalString(System::String ^ s, string& os) {
	using namespace Runtime::InteropServices;
	const char* chars =
		(const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
	os = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
}
textAdder::textAdder(System::String ^ workingPath,int postalCards)
{
	workingDir = gcnew System::String(workingPath);
	postalCardsInfo = new vector<postalCardStruct*>();
	char xmlfilePath[250];
	std::string workingDirStdStr;
	MarshalString(workingDir, workingDirStdStr);

	for (size_t i = 0; i < postalCards; i++)
	{
		sprintf_s(xmlfilePath, "%s\\c%d.xml", workingDirStdStr.c_str(), i + 1);
		postalCardStruct * cardInfoTemp = new postalCardStruct();
		FileStorage fs(xmlfilePath, FileStorage::READ);
		cardInfoTemp->cardPath = workingDirStdStr +"\\"+ fs["cardPath"];
		cardInfoTemp->xPosition = fs["xPosition"];
		cardInfoTemp->yPosition = fs["yPosition"];
		cardInfoTemp->width = fs["width"];
		cardInfoTemp->height = fs["height"];
		postalCardsInfo->push_back(cardInfoTemp);
		fs.release();

	}
	fontName = gcnew System::String(workingDir + "\\" + "IranNastaliq.ttf");
	System::Drawing::Text::PrivateFontCollection ^pfcoll = gcnew  System::Drawing::Text::PrivateFontCollection();
	//put a font file under a Fonts directory within your application root

	//pfcoll->AddFontFile(L"~/Fonts/" + fontName);
	pfcoll->AddFontFile( fontName);
	//pfcoll->Families
	ff =  (pfcoll->Families[0]);
	
}

bool textAdder::addText(Bitmap ^bitmap,System::String ^text,PointF position,int fontSize){
	return addText(bitmap,text,position,fontSize,gcnew SolidBrush(Color::Black));
}



Bitmap ^ textAdder::addText(System::String ^Signature, System::String ^ text, int cardNumber){
	Bitmap ^ bmp;

	try{
		bmp = gcnew Bitmap(gcnew System::String((*postalCardsInfo)[cardNumber]->cardPath.c_str()));

		RectangleF bodyRect(
			130,130,//x and y
			540,350 //width and height
			);



		StringFormat ^bodyStringFormat = gcnew StringFormat();
		bodyStringFormat->Alignment = StringAlignment::Far;
		bodyStringFormat->LineAlignment = StringAlignment::Near;


		System::Drawing::Font ^font = gcnew System::Drawing::Font(ff, 20, FontStyle::Bold);

		System::Drawing::Graphics ^graphics = System::Drawing::Graphics::FromImage(bmp);
		graphics->DrawString(text, font, gcnew SolidBrush(Color::Black), bodyRect, bodyStringFormat);



		StringFormat ^stringFormat = gcnew StringFormat();
		stringFormat->Alignment = StringAlignment::Center;
		stringFormat->LineAlignment = StringAlignment::Near;

		RectangleF myrect(
			(*postalCardsInfo)[cardNumber]->xPosition, (*postalCardsInfo)[cardNumber]->yPosition,
			(*postalCardsInfo)[cardNumber]->width, (*postalCardsInfo)[cardNumber]->height
			);

		/*
		System::Drawing::Font ^font = gcnew System::Drawing::Font(ff, 20, FontStyle::Bold);
		//font->Height = 20;
		System::Drawing::Graphics ^graphics = System::Drawing::Graphics::FromImage(bmp);
		PointF textPosition((*postalCardsInfo)[cardNumber]->xPosition, (*postalCardsInfo)[cardNumber]->yPosition);
		*/
		graphics->DrawString(Signature, font, gcnew SolidBrush(Color::Black), myrect, stringFormat);
		return bmp;
	}
	catch (System::Exception ^e){
		return bmp;
	}
}

Bitmap ^ textAdder::addText(System::String ^Signature, int cardNumber){
	Bitmap ^ bmp; 

	try{
		bmp	= gcnew Bitmap(gcnew System::String((*postalCardsInfo)[cardNumber]->cardPath.c_str()));

		StringFormat ^stringFormat = gcnew StringFormat();
		stringFormat->Alignment = StringAlignment::Center;
		stringFormat->LineAlignment = StringAlignment::Near;
		
		RectangleF myrect(
			(*postalCardsInfo)[cardNumber]->xPosition, (*postalCardsInfo)[cardNumber]->yPosition,
			(*postalCardsInfo)[cardNumber]->width, (*postalCardsInfo)[cardNumber]->height
			);

		
		System::Drawing::Font ^font = gcnew System::Drawing::Font(ff, 20, FontStyle::Bold);
			//font->Height = 20;
			System::Drawing::Graphics ^graphics = System::Drawing::Graphics::FromImage(bmp);
			PointF textPosition((*postalCardsInfo)[cardNumber]->xPosition, (*postalCardsInfo)[cardNumber]->yPosition );
			graphics->DrawString(Signature, font, gcnew SolidBrush(Color::Black), myrect, stringFormat);


		return bmp;
	}
	catch (System::Exception ^e){
		return bmp;
	}
	
}
bool textAdder::addText(Bitmap ^bitmap,System::String ^text,PointF position, int fontSize ,  Brush ^brush){
	
	try{
		System::Drawing::Font ^font = gcnew System::Drawing::Font(ff, fontSize, FontStyle::Bold);
		System::Drawing::Graphics ^graphics = System::Drawing::Graphics::FromImage(bitmap);		
		graphics->DrawString(text, font, brush, position);
		
		return true;
	}
	catch (System::Exception ^e){
		return false;
	}
}
