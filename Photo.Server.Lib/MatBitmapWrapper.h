 #pragma once
#include <opencv2\opencv.hpp>
#include <fstream>
#include <ctime>
 namespace PhotoServerLib {

	using namespace System;
	using namespace System::Drawing;
	public ref class MatBitmapWrapper
	{
	private:
		char       *timeBuf;
		std::string *workingDir;
	public:
		
		Bitmap ^ Mat2Bmp(cv::Mat matImg);
		cv::Mat Bmp2Mat(Bitmap ^bmpImg);
		MatBitmapWrapper(std::string workingPath);
	};

}