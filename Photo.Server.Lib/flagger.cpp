#include "StdAfx.h"
#include "flagger.h"

flagger::flagger(string workingPath, int flagNumbers)
{
	this->workingDir = workingPath;
	char xmlfilePath[250];
	for (int i = 0; i<flagNumbers; i++)
	{
		sprintf_s(xmlfilePath, "%s\\f%d.xml",workingPath.c_str(), i + 1);
		flagSpec *fs = new flagSpec();
		loadFlags(xmlfilePath, fs);
		fs->faceImg = imread(workingPath+"\\"+fs->faceImgFilePath, CV_LOAD_IMAGE_UNCHANGED | CV_LOAD_IMAGE_ANYDEPTH);
		fs->foreHeadImg = imread(workingPath + "\\" + fs->foreHeadFilePath, CV_LOAD_IMAGE_UNCHANGED | CV_LOAD_IMAGE_ANYDEPTH);
		flags.push_back(fs);
	}


}


flagger::~flagger(void)
{
}

void flagger::loadFlags(string XMLFilePath, flagSpec *flagData){
	FileStorage fs(XMLFilePath, FileStorage::READ);

	flagData->faceImgFilePath = fs["faceFilePath"];
	flagData->foreHeadFilePath = fs["foreHeadFilePath"];
	flagData->faceRatio = fs["faceRatio"];
	flagData->foreHeadRatio = fs["foreHeadRatio"];
	flagData->xFaceOffset = fs["xFaceOffset"];
	flagData->yFaceOffset = fs["yFaceOffset"];
	flagData->xForeHeadOffset = fs["xForeHeadOffset"];
	flagData->yForeHeadOffset = fs["yForeHeadOffset"];
	fs.release();
	return;
}

/*
void flagger::addFlag(Mat *img, cv::Rect flagBox, int hatNumber){

	double headWidth = (double)flagBox.width*flags[hatNumber]->Ratio;
	flagI = flags[hatNumber]->faceImg.clone();
	double ratio = (double)headWidth / (double)flagI.cols;
	resize(flagI, flagI, cv::Size(), ratio, ratio);
	int hstart = flagBox.y + flagI.rows*flags[hatNumber]->yoffset;
	int wstart = flagBox.x - (flagI.cols - flagBox.width) / 2 + flagI.cols*flags[hatNumber]->xoffset;
	int hstartDiff = 0;
	if (hstart <0)
	{
		hstartDiff = 0 - hstart;
	}

	for (int i = hstart + hstartDiff, ii = hstartDiff; ii<flagI.rows; i++, ii++)
	{
		for (int j = wstart, jj = 0; jj<flagI.cols; j++, jj++)
		{
			if (flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 3] >50)
			{
				img->data[(i*img->cols + j) * 3] = flagI.data[(ii*flagI.cols + jj)*flagI.channels()];
				img->data[(i*img->cols + j) * 3 + 1] = flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 1];
				img->data[(i*img->cols + j) * 3 + 2] = flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 2];
			}

		}
	}

}
*/

void flagger::addFaceFlag(Mat *img, cv::Rect flagBox, int faceNumber, int lr=0){

	double headWidth = (double)flagBox.width*flags[faceNumber]->faceRatio;
	flagI = flags[faceNumber]->faceImg.clone();
	double ratio = (double)headWidth / (double)flagI.cols;
	resize(flagI, flagI, cv::Size(), ratio, ratio);
	if (lr == 1)//left flag needs flipping
	{
		flip(flagI, flagI, 1);
	}
	int hstart = flagBox.y + flagI.rows*flags[faceNumber]->yFaceOffset + flagBox.height / 2;
	int wstart = flagBox.x - (flagI.cols - flagBox.width) / 2 + flagI.cols*flags[faceNumber]->xFaceOffset;
	int hstartDiff = 0;
	if (hstart <0)
	{
		hstartDiff = 0 - hstart;
	}

	for (int i = hstart + hstartDiff, ii = hstartDiff; ii<flagI.rows; i++, ii++)
	{
		for (int j = wstart, jj = 0; jj<flagI.cols; j++, jj++)
		{
			if (flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 3] >50)
			{
				
				img->data[(i*img->cols + j) * 3] = ChannelBlend_Overlay(flagI.data[(ii*flagI.cols + jj)*flagI.channels()], img->data[(i*img->cols + j) * 3]);
				img->data[(i*img->cols + j) * 3 + 1] = ChannelBlend_Overlay(flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 1], img->data[(i*img->cols + j) * 3 + 1]);
				img->data[(i*img->cols + j) * 3 + 2] = ChannelBlend_Overlay(flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 2], img->data[(i*img->cols + j) * 3 + 2]);
			}

		}
	}

}

void flagger::addForeHeadFlag(Mat *img, cv::Rect flagBox, int foreHeadNumber){

	double headWidth = (double)flagBox.width*flags[foreHeadNumber]->foreHeadRatio;
	flagI = flags[foreHeadNumber]->foreHeadImg.clone();
	double ratio = (double)headWidth / (double)flagI.cols;
	resize(flagI, flagI, cv::Size(), ratio, ratio);
	int hstart = flagBox.y + flagI.rows*flags[foreHeadNumber]->yForeHeadOffset + flagBox.height / 2;
	int wstart = flagBox.x - (flagI.cols - flagBox.width) / 2 + flagI.cols*flags[foreHeadNumber]->xForeHeadOffset;
	int hstartDiff = 0;
	if (hstart <0)
	{
		hstartDiff = 0 - hstart;
	}

	for (int i = hstart + hstartDiff, ii = hstartDiff; ii<flagI.rows; i++, ii++)
	{
		for (int j = wstart, jj = 0; jj<flagI.cols; j++, jj++)
		{
			if (flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 3] >50)
			{

				img->data[(i*img->cols + j) * 3] = ChannelBlend_Overlay(flagI.data[(ii*flagI.cols + jj)*flagI.channels()], img->data[(i*img->cols + j) * 3]);
				img->data[(i*img->cols + j) * 3 + 1] = ChannelBlend_Overlay(flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 1], img->data[(i*img->cols + j) * 3 + 1]);
				img->data[(i*img->cols + j) * 3 + 2] = ChannelBlend_Overlay(flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 2], img->data[(i*img->cols + j) * 3 + 2]);
			}

		}
	}

}

/*

void flagger::addFlag3(Mat *img, cv::Rect flagBox, int flagNumber){

	double headWidth = (double)flagBox.width*flags[flagNumber]->Ratio;
	flagI = flags[flagNumber]->faceImg.clone();
	Mat flag2 = imread("data/p2_2.jpg");



	double ratio = (double)headWidth / (double)flagI.cols;
	resize(flagI, flagI, cv::Size(), ratio, ratio);
	cvtColor(flagI, flagDist, CV_RGBA2GRAY);
	resize(flag2, flag2, cv::Size(), ratio, ratio);

	for (int i = 0; i<flagI.rows; i++)
	{
		for (int j = 0; j<flagI.cols; j++)
		{
			flagDist.data[i*flagDist.step + j*flagDist.channels()] = flagI.data[i*flagI.step + j*flagI.channels() + 3];
		}
	}
	//cvtColor(flagI, flagI, CV_RGBA2RGB);

	//Mat src_mask = 255 * Mat::ones(hatI.rows, hatI.cols, hatI.depth());
	threshold(flagDist, flagDist, 1, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

	copyMakeBorder(flagDist, flagDist, 5, 5, 5, 5, BORDER_CONSTANT, 0);
	copyMakeBorder(flag2, flag2, 5, 5, 5, 5, BORDER_CONSTANT, 0);
	//copyMakeBorder(maskEyes, maskEyes, 2, 2, 2, 2, BORDER_CONSTANT, 0);

	int erosion_size = 3;
	//dilation
	Mat element = getStructuringElement(MORPH_ELLIPSE,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));
	dilate(flagDist, flagDist, element);
#ifdef _DEBUG
	imshow("hatMask", flagDist);
	imshow("flag", flagI);
	imwrite("data/flag.jpg", flagI);

	waitKey(1);
#endif
	int x = flagBox.x + flagBox.width / 2;// +flagBox.width*flags[flagNumber]->xoffset;
	int y = flagBox.y + flagBox.height / 2;// +flagBox.height*flags[flagNumber]->yoffset;
	cv::Point center(x, y);

	seamlessClone(flag2, *img, flagDist, center, *img, NORMAL_CLONE);
	//circle(*img, center, 10, Scalar(255, 255, 255), 5);
}
*/
int flagger::test(Mat imgscr, Mat *imgdes, int flagNumber){
	
	try{

		float landmarks[2 * stasm_NLANDMARKS];
		std::string path = workingDir + "\\0.jpg";
		cvtColor(imgscr,img,CV_RGB2GRAY);
		//imgdes = imgscr.clone();

		if (!img.data)
		{
			throw("Cannot load the image \n");
			//exit(1);
		}

		if (!stasm_init(workingDir.c_str(), 0 /*trace*/))
			return -1;


		if (!stasm_open_image((const char*)img.data, img.cols, img.rows, path.c_str(),
			1 /*multiface*/, 10 /*minwidth*/))
			return -1;
			//error("stasm_open_image failed: ", stasm_lasterr());

	
		int foundface;
		 // x,y coords (note the 2)

		int nfaces = 0;
		while (1)
		{
			if (!stasm_search_auto(&foundface, landmarks))
				return -1;
				//error("stasm_search_auto failed: ", stasm_lasterr());

			if (!foundface)
				break;      // note break

			// for demonstration, convert from Stasm 77 points to XM2VTS 68 points
			stasm_convert_shape(landmarks, 77);

			/*
			// draw the landmarks on the image as white dots
			stasm_force_points_into_image(landmarks, img.cols, img.rows);
			for (int i = 0; i < stasm_NLANDMARKS; i++)
				img(cvRound(landmarks[i * 2 + 1]), cvRound(landmarks[i * 2])) = 255;
			*/
			nfaces++;
		

			/*
		int foundface;
		float landmarks[2 * stasm_NLANDMARKS]; // x,y coords (note the 2)

		if (!stasm_search_single(&foundface, landmarks,
			(const char*)img.data, img.cols, img.rows, path, "data"))
		{
			throw ("Error in stasm: %s \n",stasm_lasterr());
			//exit(1);
		}
		*/

		if (!foundface)
			throw("No face found in %s\n", path);
		else
		{
			
			faceRect=makeFaceBox(landmarks);
			cv::Point pFace(faceRect.x,faceRect.y);			
			


			//Left flag
			faceEffectMask = Mat(faceRect.height, faceRect.width, CV_8U, 0.0);
			int index1[4] = { 2, 38, 50, 59 };
			int ilen = 4;
#ifdef _DEBUG
			for (int i = 0; i < ilen; i++){
				line(img, cv::Point(cvRound(landmarks[index1[i] * 2]), cvRound(landmarks[index1[i] * 2 + 1])),
					cv::Point(cvRound(landmarks[(index1[i] + 1) * 2]), cvRound(landmarks[(index1[i] + 1) * 2 + 1])),
					cv::Scalar(255,255,255),2);
			}

#endif
			ROI_Poly.clear();
			ROI_Vertices.clear();
			for (int i = 0; i < ilen; i++){
				ROI_Vertices.push_back(cv::Point(cvRound(landmarks[index1[i] * 2]), cvRound(landmarks[index1[i] * 2 + 1])) - pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(faceEffectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);
			Rect leftFlagRect = boundingRect(faceEffectMask);
			leftFlagRect.x += faceRect.x;
			leftFlagRect.y += faceRect.y;
			rectangle(img, leftFlagRect, Scalar(255, 0, 0), 2);
			addFaceFlag(imgdes, leftFlagRect, (int)flagNumber-1,1);


			//Right flag 
			
			faceEffectMask = Mat(faceRect.height, faceRect.width, CV_8U, 0.0);
			int index1R[4] = { 12, 48, 55, 66 };
			//int ilen = 4;
#ifdef _DEBUG
			for (int i = 0; i < ilen; i++){
				line(img, cv::Point(cvRound(landmarks[index1R[i] * 2]), cvRound(landmarks[index1R[i] * 2 + 1])),
					cv::Point(cvRound(landmarks[(index1R[i] + 1) * 2]), cvRound(landmarks[(index1R[i] + 1) * 2 + 1])),
					cv::Scalar(255, 255, 255), 2);
			}

#endif
			ROI_Poly.clear();
			ROI_Vertices.clear();
			for (int i = 0; i < ilen; i++){
				ROI_Vertices.push_back(cv::Point(cvRound(landmarks[index1R[i] * 2]), cvRound(landmarks[index1R[i] * 2 + 1])) - pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(faceEffectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);
			Rect rightFlagRect = boundingRect(faceEffectMask);
			rightFlagRect.x += faceRect.x;
			rightFlagRect.y += faceRect.y;
			rectangle(img, rightFlagRect, Scalar(255, 0, 0), 2);
			addFaceFlag(imgdes, rightFlagRect, flagNumber-1);


			//headFlag
			foreHeadEffectMask = Mat(faceRect.height, faceRect.width, CV_8U, 0.0);

			int index2[4] = { 13, 15, 17, 24 };
			//int ilen = 4;
#ifdef _DEBUG
			for (int i = 0; i < ilen; i++){
				line(img, cv::Point(cvRound(landmarks[index2[i] * 2]), cvRound(landmarks[index2[i] * 2 + 1])),
					cv::Point(cvRound(landmarks[(index2[i] + 1) * 2]), cvRound(landmarks[(index2[i] + 1) * 2 + 1])),
					cv::Scalar(255, 255, 255), 2);
			}

#endif
			ROI_Poly.clear();
			ROI_Vertices.clear();
			for (int i = 0; i < ilen; i++){
				ROI_Vertices.push_back(cv::Point(cvRound(landmarks[index2[i] * 2]), cvRound(landmarks[index2[i] * 2 + 1])) - pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(foreHeadEffectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);
			Rect foreHeadFlagRect = boundingRect(foreHeadEffectMask);
			foreHeadFlagRect.x += faceRect.x;
			foreHeadFlagRect.y += faceRect.y;
			rectangle(img, foreHeadFlagRect, Scalar(255, 255, 0), 2);
			addForeHeadFlag(imgdes, foreHeadFlagRect, flagNumber-1);



			//Mat ffo =(*imgdes)(flagRect);
			//blur(ffo, ffo, Size(5, 5));
			//Mat flag = imread("data\pf.png");


			/*
#ifdef _DEBUG
			for (int i = 16; i < 21; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif

			ROI_Poly.clear();
			ROI_Vertices.clear();
			for (int i = 16; i < 22; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);                 

#ifdef _DEBUG
			for (int i = 22; i <27 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif

			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 22; i < 28; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);

			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);   

#ifdef _DEBUG
			for (int i = 30; i <38 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif


			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 30; i < 38; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  

#ifdef _DEBUG
			for (int i = 39; i <47 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif

			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 40; i < 47; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  

#ifdef _DEBUG
			for (int i = 59; i <76 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif
			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 72; i < 77; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			for (int i = 59; i < 66; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  

			*/
				
			
#ifdef _DEBUG
			imshow("FACEMASKDEBUG", faceEffectMask);
			imshow("FOREHEADEFFECTMASK", foreHeadEffectMask);
			imshow("IMG ASM", img);
#endif
		}

		//*runtime = *runtime - timeGetTime();
#ifdef _DEBUG
		cv::imshow("desImageDEUG", *imgdes);
		cv::imshow("sourceIMGDebug", imgscr);	
		cv::imshow("ASMDEBUG", img);	
		cv::waitKey(1);
#endif
		}
		return nfaces;
	}
	catch(std::exception e) {
		e.what();
		return -1;
	}
	

}