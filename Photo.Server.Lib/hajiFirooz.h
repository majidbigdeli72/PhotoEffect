#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "opencv/highgui.h"
#include "stasm/stasm_lib.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include <Windows.h>
#include <fstream>
#include <ctime>

using namespace cv;
using namespace std;
#define makeFaceBox(landmarks) cv::Rect(landmarks[0],landmarks[14*2+1],landmarks[11*2]-landmarks[0],landmarks[6*2+1]-landmarks[14*2+1]);
//#define headHatRatio 1.3
struct hatSpec{
	double headHatRation;
	double xoffset,yoffset;
	string filePath;
	Mat hatImg;
};

class hajiFirooz
{
public:
	//Mat *im;
	hajiFirooz(string workingPath, int hatNumbers); //number of hats
	bool makeFirooz(Mat imgscr,Mat *imgdes,int hatType, int darknessType); //faceNumbers  1: single face   2: multiface

private:
	double rad2deg(double rad);
	char       timeBuf[100];
	void addBeard2(Mat *img, cv::Rect faceBox, float *landmarks);
	void addLeftEyeb2(Mat *img, cv::Rect faceBox, float *landmarks);
	void addRightEyeb2(Mat *img, cv::Rect faceBox, float *landmarks);
	void rotate(cv::Mat& src, cv::Mat& dst, double angle);
	void loadHat(string XMLFilePath,hatSpec *hatData);
	void makeFiroozDistRGB(Mat *img,Mat *mask, cv::Rect faceBox,int darkLevel);
	void addHat(Mat *img, cv::Rect faceBox,int hatNumber);//hat number between 0 and N, first hat is 0
	void addHat2(Mat *img, cv::Rect faceBox, int hatNumber, double rotationAngle, double rotationTrans);
	void addHat3(Mat *img, cv::Rect faceBox, int hatNumber);
	vector<hatSpec*> hats;
	//vector<Mat> hatImages;
	double getRotation(float *landmarks, Mat* img);
	double getRotation(float *landmarks);
	double getRotationTrans(float *landmarks);
	double getLips(float *landmarks, Mat* img);

	double getEybroLeft(float *landmarks, Mat* img);
	double getEybroRight(float *landmarks, Mat* img);
	vector<cv::Point> ROI_Poly;
	vector<cv::Point> ROI_Vertices;
	Mat dielement,hatI;
	Mat breadImg;
	Mat LEB, REB;
	Mat maskFace,maskEyes,maskTotal;
	Mat effectMask,imghsv;
	Mat img;
	cv::Rect faceRect;
	Mat hatDist;
	std::string workingDir;

};

