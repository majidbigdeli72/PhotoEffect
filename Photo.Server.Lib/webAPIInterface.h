
#pragma once
#include "MatBitmapWrapper.h"
#include "flagger.h"
#include "textAdder.h"
#include "hajiFirooz.h"
#include <string>  
#include <iostream>  
#include <fstream>
#include <ctime>

 namespace PhotoServerLib {
	using namespace System;
	using namespace System::Drawing;
	public ref class webAPIInterface
	{
	private:
		char       *timeBuf;
		flagger *flaggerEngine;
		MatBitmapWrapper ^mat2Bmp;
		textAdder ^ myTextAdder;
		hajiFirooz * hajiMakerEngine;
		void MarshalString(System::String ^ s, string& os);
		System::Drawing::Font ^watermarkDrawFont;
		SolidBrush ^watermarkDrawBrush;
		SolidBrush ^watermarkBackDrawBrush;
		std::string *workingDir;
	public:
		Bitmap ^ cartoofify(Bitmap ^inp, float level);
		Bitmap ^ flagIt(Bitmap ^inp, float level);//level 1: means perspolice 2:esteghlal and 3:Iran
		//Bitmap ^ addText(System::String ^text, Bitmap ^bitmap);
		Bitmap ^ addText(System::String ^signature, System::String ^text, int postalCardNumber);
		Bitmap ^ hajiMaker(Bitmap ^ bmp, int hatNumber, int darknessType);
		//webAPIInterface();
		webAPIInterface(System::String ^workingPath);
		Bitmap ^ waterMarkParmik(Bitmap ^inp);
	};

}