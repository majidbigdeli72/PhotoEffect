#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <opencv2/opencv.hpp>
namespace PhotoServerLib {
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Runtime::InteropServices;
using namespace std;
using namespace cv;
struct postalCardStruct{
	int xPosition, yPosition;
	int width, height;
	std::string cardPath;	
	
};
	public ref class textAdder
	{
	private:
		vector<postalCardStruct*> *postalCardsInfo;
		System::String ^fontName;
		FontFamily ^ff;
		System::String ^ workingDir;
		void MarshalString(System::String ^ s, string& os);
	public:
		textAdder(System::String ^ workingPath,int postalCards);
		Bitmap ^ addText(System::String ^Signature, int cardNumber);
		Bitmap ^ addText(System::String ^Signature, System::String ^ text, int cardNumber);
		bool addText(Bitmap ^bitmap, System::String ^text, PointF position, int fontSize, Brush ^brush);
		bool addText(Bitmap ^bitmap, System::String ^text, PointF position, int fontSize);
		

	};

}