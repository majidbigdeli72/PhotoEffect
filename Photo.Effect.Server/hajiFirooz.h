#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "opencv/highgui.h"
#include "stasm_lib.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include <Windows.h>

using namespace cv;
using namespace std;
#define makeFaceBox(landmarks) cv::Rect(landmarks[0],landmarks[14*2+1],landmarks[11*2]-landmarks[0],landmarks[6*2+1]-landmarks[14*2+1]);
//#define headHatRatio 1.3
struct hatSpec{
	double headHatRation;
	double xoffset,yoffset;
	string filePath;
	Mat hatImg;
};

class hajiFirooz
{
public:
	//Mat *im;
	hajiFirooz(int hatNumbers); //number of hats
	bool makeFirooz(Mat imgscr,Mat imgdes,int faceNumbers,int hatType, int darknessType,int *runtime); //faceNumbers  1: single face   2: multiface  hatType: between 1 and 3  darkNessType: between 1(dark) and 3(light)
	bool makeFirooz(Mat imgscr,Mat imgdes,int faceNumbers,int hatType, int darknessType); //faceNumbers  1: single face   2: multiface

private:
	void loadHat(string XMLFilePath,hatSpec *hatData);
	void makeFiroozDistRGB(Mat *img,Mat *mask, cv::Rect faceBox,int darkLevel);
	void addHat(Mat *img, cv::Rect faceBox,int hatNumber);//hat number between 0 and N, first hat is 0
	void addHat2(Mat *img, cv::Rect faceBox,int hatNumber);
	void addHat3(Mat *img, cv::Rect faceBox, int hatNumber);
	vector<hatSpec*> hats;
	//vector<Mat> hatImages;


	vector<cv::Point> ROI_Poly;
	vector<cv::Point> ROI_Vertices;
	Mat dielement,hatI;

	Mat maskFace,maskEyes,maskTotal;
	Mat effectMask,img,imghsv;
	cv::Rect faceRect;
	Mat hatDist;


};

