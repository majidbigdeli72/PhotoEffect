// minimal.cpp: Display the landmarks of a face in an image.
//              This demonstrates stasm_search_single.

#include <stdio.h>
#include <stdlib.h>
#include "opencv/highgui.h"
#include "stasm_lib.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include <Windows.h>
using namespace std;
using namespace cv;
#pragma comment(lib,"Winmm.lib")
#define makeFaceBox(landmarks) cv::Rect(landmarks[0],landmarks[14*2+1],landmarks[11*2]-landmarks[0],landmarks[6*2+1]-landmarks[14*2+1]);
#define darkingLevel 100
#define headHatRatio 1.5
Mat maskFace;
Mat maskEyes;
Mat maskTotal;


void makeFiroozDistRGB(Mat *img,Mat *mask, cv::Rect faceBox){
 	
	maskFace = mask->clone();
	maskEyes = mask->clone();
	
	maskFace += 255-100;
	threshold(maskFace,maskFace, 250, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	maskFace=255-maskFace;
	threshold(maskEyes,maskEyes, 250, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

	copyMakeBorder(maskFace,maskFace,2,2,2,2,BORDER_CONSTANT,255);
	copyMakeBorder(maskEyes,maskEyes,2,2,2,2,BORDER_CONSTANT,0);
	
	int erosion_size=faceBox.width/4;
	//dilation
	Mat element = getStructuringElement( MORPH_ELLIPSE,
		Size( 2*erosion_size + 1, 2*erosion_size+1 ),
		Point( erosion_size, erosion_size ) );
	dilate(maskFace, maskFace, element );
	
	//cv::close
	
	cv::distanceTransform(maskFace,maskFace,CV_DIST_C,3 );
	cv::distanceTransform(maskEyes,maskEyes,CV_DIST_C,3 );
	
	//dist *= 5;
	normalize(maskFace, maskFace, 0, 1., NORM_MINMAX);
	normalize(maskEyes, maskEyes, 0, 1., NORM_MINMAX);
	//maskFace =maskFace & *mask;
	
	/*
	threshold(maskFace, maskFace, .5, 1., CV_THRESH_BINARY);
 	cv::distanceTransform(maskFace,maskFace,CV_DIST_C,3 );
	normalize(maskFace, maskFace, 0, 1., NORM_MINMAX);
	*/
	//maskTotal = maskEyes;

	//maskTotal = (maskFace) |  maskEyes;
	//cvtColor(maskTotal,maskTotal,CV_RGB2GRAY);
#ifdef _DEBUG
	imshow("distFace",maskFace);
	imshow("distEyes",maskEyes);
	//imshow("distTotal",maskTotal);
	waitKey(1);
#endif
	float levelDif=(float)faceBox.width/400.0;
	int ii ,jj;
	for (int i=faceBox.y, ii=0;i<faceBox.y+faceBox.height;i++,ii++)
	{
		for (int j = faceBox.x, jj=0;j<faceBox.x+faceBox.width;j++,jj++)
		{
			if(mask->data[ii*mask->cols+jj]>1) 
			{
				float integLevelE= 1- maskEyes.at<float>(ii,jj);
				integLevelE-=levelDif;
				float integLevelF= 1- maskFace.at<float>(ii,jj);
				integLevelF-=levelDif;
				if (integLevelE > integLevelF)
				{
					int newValR = (1-integLevelF) * (float) img->data[(i*img->cols+j)*3+2]+
						(integLevelF) * (float)64.0;
					int newValG = (1- integLevelF) * (double)img->data[(i*img->cols+j)*3+1]+ 
						(integLevelF) * 32.0;
					if (newValR <255  && newValR >0)
					{
						img->data[(i*img->cols+j)*3+2]=newValR;
					}
					else{
						if (newValR >=255)
							img->data[(i*img->cols+j)*3+2]=255;
						else
							img->data[(i*img->cols+j)*3+2] =0;
					}
					if (newValG <255  && newValG >0)
					{
						img->data[(i*img->cols+j)*3+1]=newValG;
					}
					else{
						if (newValG >=255)
							img->data[(i*img->cols+j)*3+1]=255;
						else
							img->data[(i*img->cols+j)*3+1] =0;
					}
				}
				else{
					int newValR = (1-integLevelE) * (float) img->data[(i*img->cols+j)*3+2]+
						(integLevelE) * (float)64.0;
					int newValG = (1- integLevelE) * (double)img->data[(i*img->cols+j)*3+1]+ 
						(integLevelE) * 32.0;
					if (newValR <255  && newValR >0)
					{
						img->data[(i*img->cols+j)*3+2]=newValR;
					}
					else{
						if (newValR >=255)
							img->data[(i*img->cols+j)*3+2]=255;
						else
							img->data[(i*img->cols+j)*3+2]=0;
					}
					if (newValG <255  && newValG >0)
					{
						img->data[(i*img->cols+j)*3+1]=newValG;
					}
					else{
						if (newValG >=255)
							img->data[(i*img->cols+j)*3+1]=255;
						else
							img->data[(i*img->cols+j)*3+1]=0;
					}

				}
				
				

				//uchar darkingLevel = mask->data[ii*mask->cols+jj];
				/*
				if (mask->data[ii*mask->cols+jj]==100 )
				{
					if(img->data[(i*img->cols+j)*3]-darkingLevel>0)
						img->data[(i*img->cols+j)*3]-=darkingLevel;
					else
						img->data[(i*img->cols+j)*3]=0;

					if(img->data[(i*img->cols+j)*3+1]-darkingLevel>0)
						img->data[(i*img->cols+j)*3+1]-=darkingLevel;
					else
						img->data[(i*img->cols+j)*3+1]=0;

					if(img->data[(i*img->cols+j)*3+2]-darkingLevel>0)	
						img->data[(i*img->cols+j)*3+2]-=darkingLevel;
					else
						img->data[(i*img->cols+j)*3+2]=0;
				}
				*/
			}

			
		}

	}
	//img->adjustROI(faceBox.y,faceBox.y+faceBox.height,faceBox.x,faceBox.x+faceBox.width);
	//for		
}



void makeFirooz(Mat *img,Mat *mask, cv::Rect faceBox){
	//Mat face= (*img)(faceBox);
	//imshow("faceROI",face);
	int ii ,jj;
	for (int i=faceBox.y, ii=0;i<faceBox.y+faceBox.height;i++,ii++)
	{
		for (int j = faceBox.x, jj=0;j<faceBox.x+faceBox.width;j++,jj++)
		{
			if (mask->data[ii*mask->cols+jj]==100 )
			{
				if(img->data[(i*img->cols+j)*3]-darkingLevel>0)
					img->data[(i*img->cols+j)*3]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3]=0;
				
				if(img->data[(i*img->cols+j)*3+1]-darkingLevel>0)
					img->data[(i*img->cols+j)*3+1]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3+1]=0;

				if(img->data[(i*img->cols+j)*3+2]-darkingLevel>0)	
					img->data[(i*img->cols+j)*3+2]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3+2]=0;
			}
		}
		
	}
	//img->adjustROI(faceBox.y,faceBox.y+faceBox.height,faceBox.x,faceBox.x+faceBox.width);
	//for		
}



void makeFiroozHSV(Mat *img,Mat *mask, cv::Rect faceBox){

	//Mat face= (*img)(faceBox);
	//imshow("faceROI",face);
	int ii ,jj;
	for (int i=faceBox.y, ii=0;i<faceBox.y+faceBox.height;i++,ii++)
	{
		for (int j = faceBox.x, jj=0;j<faceBox.x+faceBox.width;j++,jj++)
		{
			if (mask->data[ii*mask->cols+jj]==100 )
			{
				/*
				if(img->data[(i*img->cols+j)*3]-darkingLevel>0)
					img->data[(i*img->cols+j)*3]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3]=0;
				
				if(img->data[(i*img->cols+j)*3+1]-darkingLevel>0 && img->data[(i*img->cols+j)*3+1]+darkingLevel<255)
					img->data[(i*img->cols+j)*3+1]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3+1]=0;
					*/
				
				if(img->data[(i*img->cols+j)*3+2]-darkingLevel>0)	
					img->data[(i*img->cols+j)*3+2]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3+2]=0;
					
			}
		}

	}
	//img->adjustROI(faceBox.y,faceBox.y+faceBox.height,faceBox.x,faceBox.x+faceBox.width);
	//for		
}

void addHat(Mat *img,Mat* hat , cv::Rect faceBox){
	double headWidth = (double)faceBox.width*headHatRatio;
	double ratio = (double)headWidth/(double)hat->cols;
	resize(*hat,*hat,cv::Size(),ratio,ratio);
	for (int i=faceBox.y-hat->rows +hat->rows*0.2, ii=0;ii<hat->rows;i++,ii++)
	{
		for (int j = faceBox.x -(hat->cols-faceBox.width)/2 , jj=0;jj<hat->cols;j++,jj++)
		{
			if (hat->data[(ii*hat->cols+jj)*hat->channels()+3 ] >0)
			{
				img->data[(i*img->cols+j)*3] = hat->data[(ii*hat->cols+jj)*hat->channels()];
				img->data[(i*img->cols+j)*3+1] = hat->data[(ii*hat->cols+jj)*hat->channels()+1];
				img->data[(i*img->cols+j)*3+2] = hat->data[(ii*hat->cols+jj)*hat->channels()+2];
			}
			
		}
	}
}

int main()
{
    //static const char* const path = "../data/testface.jpg";
	static const char* const path = "../data/0.jpg";
	static const char* const path2 = "../data/alaki.jpg";
    cv::Mat img2(cv::imread(path, CV_LOAD_IMAGE_COLOR));
	//resize(img2,img2,Size(),0.2,0.2);
	cv::Mat hat = imread("../data/f1_1.png",CV_LOAD_IMAGE_UNCHANGED | CV_LOAD_IMAGE_ANYDEPTH);

	Mat_<unsigned char> img;
	cvtColor(img2,img,CV_RGB2GRAY);
	int tt = timeGetTime();
	Mat im2 = img2.clone();
	Mat imghsv = im2.clone();
	cvtColor(im2,imghsv,CV_RGB2HSV);

    if (!img.data)
    {
        printf("Cannot load %s\n", path);
        exit(1);
    }

    int foundface;
    float landmarks[2 * stasm_NLANDMARKS]; // x,y coords (note the 2)
	
    if (!stasm_search_single(&foundface, landmarks,
                             (const char*)img.data, img.cols, img.rows, path2, "../data"))
    {
        printf("Error in stasm_search_single: %s\n", stasm_lasterr());
        exit(1);
    }

    if (!foundface)
         printf("No face found in %s\n", path);
    else
    {
        // draw the landmarks on the image as white dots (image is monochrome)
        stasm_force_points_into_image(landmarks, img.cols, img.rows);
        for (int i = 0; i < stasm_NLANDMARKS; i++)
            img(cvRound(landmarks[i*2+1]), cvRound(landmarks[i*2])) = 255;
		cv::Rect faceRect=makeFaceBox(landmarks);
		//cv::rectangle(im2,faceRect,Scalar(255,0,0),2);
		Mat effectMask = Mat(faceRect.height,faceRect.width,CV_8U,0.0);
	

		
		vector<cv::Point> ROI_Poly;
		vector<cv::Point> ROI_Vertices;

		for (int i = 0; i < 15; i++){
			line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
				cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
				cv::Scalar(255,255,255),2);
		}
		cv::Point pFace(faceRect.x,faceRect.y);
		
		for (int i = 0; i < 16; i++){
			ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
		}
		approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
		fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 100, 8,0);                 

		for (int i = 16; i < 21; i++){
			line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
				cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
				cv::Scalar(255,255,255),2);
		}


		ROI_Poly.clear();
		ROI_Vertices.clear();
		for (int i = 16; i < 22; i++){
			ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
		}
		approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
		fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);                 

		for (int i = 22; i <27 ; i++){
			line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
				cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
				cv::Scalar(255,255,255),2);
		}

		ROI_Vertices.clear();
		ROI_Poly.clear();
		for (int i = 22; i < 28; i++){
			ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
		}
		approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);

		fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);   


		ROI_Vertices.clear();
		ROI_Poly.clear();
		for (int i = 30; i <38 ; i++){
			line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
				cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
				cv::Scalar(255,255,255),2);
		}


		ROI_Vertices.clear();
		ROI_Poly.clear();
		for (int i = 30; i < 38; i++){
			ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
		}
		approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
		fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  


		for (int i = 39; i <47 ; i++){
			line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
				cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
				cv::Scalar(255,255,255),2);
		}

		ROI_Vertices.clear();
		ROI_Poly.clear();
		for (int i = 40; i < 47; i++){
			ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
		}
		approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
		fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  


		//nose
		ROI_Vertices.clear();
		ROI_Poly.clear();
		for (int i = 48; i < 55; i++){
			ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
		}
		approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
		fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  


		for (int i = 59; i <76 ; i++){
			line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
				cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
				cv::Scalar(255,255,255),2);
		}

		ROI_Vertices.clear();
		ROI_Poly.clear();
		for (int i = 72; i < 77; i++){
			ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
		}
		for (int i = 59; i < 66; i++){
			ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
		}
		approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
		fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  
		
		
		int dilation_size=5;
		//dilation
		Mat element = getStructuringElement( MORPH_ELLIPSE,
			Size( 2*dilation_size + 1, 2*dilation_size+1 ),
			Point( dilation_size, dilation_size ) );
		dilate(effectMask, effectMask, element );
		
		//makeFirooz(&im2,&effectMask,faceRect);

		makeFiroozDistRGB(&imghsv,&effectMask,faceRect);
		cvtColor(imghsv,im2,CV_HSV2RGB);
		addHat(&im2,&hat,faceRect);
		imshow("mymask",effectMask);

    }

	tt = tt - timeGetTime();
	cout<<"run time is: "<<tt<<endl;
    cv::imwrite("minimal.jpg", img);
    cv::imshow("stasm minimal", img);
	 cv::imshow("eye", im2);
	cv::imshow("HSVImg",imghsv);
    cv::waitKey();
    return 0;
}
