#include "stdafx.h"
#include "webAPIInterface.h"
using namespace PhotoServer;

webAPIInterface::webAPIInterface()
{
	MatBitmapWrapper ^mat2Bmp = gcnew MatBitmapWrapper();
}


Bitmap ^ webAPIInterface::cartoofify(Bitmap ^inp, int level){
	cv::Mat img= mat2Bmp->Bmp2Mat(inp);
	cv::Mat imout = img.clone();


	cv::stylization(img, imout, 60, 0.9);
	Bitmap ^res=  mat2Bmp->Mat2Bmp(imout);

	imout.release();
	img.release();
	return res;


}
