#include "StdAfx.h"
#include "hajiFirooz.h"
static const char* const path = "data/0.jpg";

void hajiFirooz::loadHat(string XMLFilePath,hatSpec *hatData){
	FileStorage fs(XMLFilePath, FileStorage::READ);

		hatData->filePath = fs["filePath"];
		hatData->headHatRation = fs["hatRatio"];
		hatData->xoffset = fs["xoffset"];
		hatData->yoffset = fs["yoffset"];

	fs.release();
	return;
}

hajiFirooz::hajiFirooz(int hatNumbers)
{
	char xmlfilePath[100];
	for (int i=0;i<hatNumbers;i++)
	{
		sprintf_s(xmlfilePath,"data/h%d.xml",i+1);
		hatSpec *hs = new hatSpec(); 
		loadHat(xmlfilePath,hs);
		hs->hatImg = imread(hs->filePath,CV_LOAD_IMAGE_UNCHANGED | CV_LOAD_IMAGE_ANYDEPTH);
		hats.push_back(hs);
	}

/*

	hat1 = imread("data/f9.png",CV_LOAD_IMAGE_UNCHANGED | CV_LOAD_IMAGE_ANYDEPTH);
	hat2 = imread("data/f1_1.png",CV_LOAD_IMAGE_UNCHANGED | CV_LOAD_IMAGE_ANYDEPTH);
	hat3 = imread("data/f2.png",CV_LOAD_IMAGE_UNCHANGED | CV_LOAD_IMAGE_ANYDEPTH);

*/
	


	int dilation_size=5;
	//dilation
	dielement = getStructuringElement( MORPH_ELLIPSE,
		Size( 2*dilation_size + 1, 2*dilation_size+1 ),
		Point( dilation_size, dilation_size ) );
		
}




void hajiFirooz::makeFiroozDistRGB(Mat *img,Mat *mask, cv::Rect faceBox,int darkLevel){
 	
	maskFace = mask->clone();
	maskEyes = mask->clone();
	
	maskFace += 255-100;
	threshold(maskFace,maskFace, 250, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	maskFace=255-maskFace;
	threshold(maskEyes,maskEyes, 250, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

	copyMakeBorder(maskFace,maskFace,2,2,2,2,BORDER_CONSTANT,255);
	copyMakeBorder(maskEyes,maskEyes,2,2,2,2,BORDER_CONSTANT,0);
	
	int erosion_size=faceBox.width/5;
	//dilation
	Mat element = getStructuringElement( MORPH_ELLIPSE,
		Size( 2*erosion_size + 1, 2*erosion_size+1 ),
		Point( erosion_size, erosion_size ) );
	dilate(maskFace, maskFace, element );
	
	//cv::close
	
	cv::distanceTransform(maskFace,maskFace,CV_DIST_C,3 );
	cv::distanceTransform(maskEyes,maskEyes,CV_DIST_C,3 );
	
	//dist *= 5;
	normalize(maskFace, maskFace, 0, 1., NORM_MINMAX);
	normalize(maskEyes, maskEyes, 0, 1., NORM_MINMAX);
	//maskFace =maskFace & *mask;
	
	/*
	threshold(maskFace, maskFace, .5, 1., CV_THRESH_BINARY);
 	cv::distanceTransform(maskFace,maskFace,CV_DIST_C,3 );
	normalize(maskFace, maskFace, 0, 1., NORM_MINMAX);
	/ */
	//maskTotal = maskEyes;

	//maskTotal = (maskFace) |  maskEyes;
	//cvtColor(maskTotal,maskTotal,CV_RGB2GRAY);
#ifdef _DEBUG
	imshow("distFace",maskFace);
	imshow("distEyes",maskEyes);
	//imshow("distTotal",maskTotal);
	waitKey(1);
#endif
	double darkval  = 16.0*darkLevel;
	float levelDif=(float)faceBox.width/600.0;
	int ii ,jj;
	for (int i=faceBox.y, ii=0;i<faceBox.y+faceBox.height;i++,ii++)
	{
		for (int j = faceBox.x, jj=0;j<faceBox.x+faceBox.width;j++,jj++)
		{
			if(mask->data[ii*mask->cols+jj]>1) 
			{
				float integLevelE= 1- maskEyes.at<float>(ii,jj);
				integLevelE-=levelDif;
				float integLevelF= 1- maskFace.at<float>(ii,jj);
				integLevelF-=levelDif;
				if (integLevelE > integLevelF)
				{
					int newValR = (1-integLevelF) * (float) img->data[(i*img->cols+j)*3+2]+
						(integLevelF) * (float)darkval;
					int newValG = (1- integLevelF) * (double)img->data[(i*img->cols+j)*3+1]+ 
						(integLevelF) * darkval;
					int newValB = (1- integLevelF) * (double)img->data[(i*img->cols+j)*3+0]+ 
						(integLevelF) * darkval;
					if (newValR <255  && newValR >0)
					{
						img->data[(i*img->cols+j)*3+2]=newValR;
					}
					else{
						if (newValR >=255)
							img->data[(i*img->cols+j)*3+2]=255;
						else
							img->data[(i*img->cols+j)*3+2] =0;
					}
					if (newValG <255  && newValG >0)
					{
						img->data[(i*img->cols+j)*3+1]=newValG;
					}
					else{
						if (newValG >=255)
							img->data[(i*img->cols+j)*3+1]=255;
						else
							img->data[(i*img->cols+j)*3+1] =0;
					}

					if (newValB <255  && newValB >0)
					{
						img->data[(i*img->cols+j)*3+0]=newValB;
					}
					else{
						if (newValB >=255)
							img->data[(i*img->cols+j)*3+0]=255;
						else
							img->data[(i*img->cols+j)*3+0] =0;
					}
				}
				else{
					int newValR = (1-integLevelE) * (float) img->data[(i*img->cols+j)*3+2]+
						(integLevelE) * (float)darkval;
					int newValG = (1- integLevelE) * (double)img->data[(i*img->cols+j)*3+1]+ 
						(integLevelE) * darkval;
					int newValB = (1- integLevelE) * (double)img->data[(i*img->cols+j)*3+0]+ 
						(integLevelE) * darkval;
					if (newValR <255  && newValR >0)
					{
						img->data[(i*img->cols+j)*3+2]=newValR;
					}
					else{
						if (newValR >=255)
							img->data[(i*img->cols+j)*3+2]=255;
						else
							img->data[(i*img->cols+j)*3+2]=0;
					}
					if (newValG <255  && newValG >0)
					{
						img->data[(i*img->cols+j)*3+1]=newValG;
					}
					else{
						if (newValG >=255)
							img->data[(i*img->cols+j)*3+1]=255;
						else
							img->data[(i*img->cols+j)*3+1]=0;
					}
					if (newValB <255  && newValB >0)
					{
						img->data[(i*img->cols+j)*3+0]=newValB;
					}
					else{
						if (newValB >=255)
							img->data[(i*img->cols+j)*3+0]=255;
						else
							img->data[(i*img->cols+j)*3+0] =0;
					}

				}
				
				

				//uchar darkingLevel = mask->data[ii*mask->cols+jj];
				/*
				if (mask->data[ii*mask->cols+jj]==100 )
				{
					if(img->data[(i*img->cols+j)*3]-darkingLevel>0)
						img->data[(i*img->cols+j)*3]-=darkingLevel;
					else
						img->data[(i*img->cols+j)*3]=0;

					if(img->data[(i*img->cols+j)*3+1]-darkingLevel>0)
						img->data[(i*img->cols+j)*3+1]-=darkingLevel;
					else
						img->data[(i*img->cols+j)*3+1]=0;

					if(img->data[(i*img->cols+j)*3+2]-darkingLevel>0)	
						img->data[(i*img->cols+j)*3+2]-=darkingLevel;
					else
						img->data[(i*img->cols+j)*3+2]=0;
				}
				// */
			}

		}

	}
	//img->adjustROI(faceBox.y,faceBox.y+faceBox.height,faceBox.x,faceBox.x+faceBox.width);
	//for
	
}

/*

void makeFirooz(Mat *img,Mat *mask, cv::Rect faceBox){
	//Mat face= (*img)(faceBox);
	//imshow("faceROI",face);
	int ii ,jj;
	for (int i=faceBox.y, ii=0;i<faceBox.y+faceBox.height;i++,ii++)
	{
		for (int j = faceBox.x, jj=0;j<faceBox.x+faceBox.width;j++,jj++)
		{
			if (mask->data[ii*mask->cols+jj]==100 )
			{
				if(img->data[(i*img->cols+j)*3]-darkingLevel>0)
					img->data[(i*img->cols+j)*3]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3]=0;
				
				if(img->data[(i*img->cols+j)*3+1]-darkingLevel>0)
					img->data[(i*img->cols+j)*3+1]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3+1]=0;

				if(img->data[(i*img->cols+j)*3+2]-darkingLevel>0)	
					img->data[(i*img->cols+j)*3+2]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3+2]=0;
			}
		}
		
	}
	//img->adjustROI(faceBox.y,faceBox.y+faceBox.height,faceBox.x,faceBox.x+faceBox.width);
	//for		
}



void makeFiroozHSV(Mat *img,Mat *mask, cv::Rect faceBox){

	//Mat face= (*img)(faceBox);
	//imshow("faceROI",face);
	int ii ,jj;
	for (int i=faceBox.y, ii=0;i<faceBox.y+faceBox.height;i++,ii++)
	{
		for (int j = faceBox.x, jj=0;j<faceBox.x+faceBox.width;j++,jj++)
		{
			if (mask->data[ii*mask->cols+jj]==100 )
			{
				/*
				if(img->data[(i*img->cols+j)*3]-darkingLevel>0)
					img->data[(i*img->cols+j)*3]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3]=0;
				
				if(img->data[(i*img->cols+j)*3+1]-darkingLevel>0 && img->data[(i*img->cols+j)*3+1]+darkingLevel<255)
					img->data[(i*img->cols+j)*3+1]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3+1]=0;
					/ * /
				
				if(img->data[(i*img->cols+j)*3+2]-darkingLevel>0)	
					img->data[(i*img->cols+j)*3+2]-=darkingLevel;
				else
					img->data[(i*img->cols+j)*3+2]=0;
					
			}
		}

	}
	//img->adjustROI(faceBox.y,faceBox.y+faceBox.height,faceBox.x,faceBox.x+faceBox.width);
	//for		
}

*/



void hajiFirooz::addHat3(Mat *img, cv::Rect faceBox, int hatNumber){

	double headWidth = (double)faceBox.width*hats[hatNumber]->headHatRation;
	hatI = hats[hatNumber]->hatImg.clone();
	
	

	double ratio = (double)headWidth / (double)hatI.cols;
	resize(hatI, hatI, cv::Size(), ratio, ratio);
	cvtColor(hatI, hatDist, CV_RGBA2GRAY);
	for (int i = 0; i<hatI.rows; i++)
	{
		for (int j = 0; j<hatI.cols; j++)
		{
			hatDist.data[i*hatDist.step + j*hatDist.channels()] = hatI.data[i*hatI.step + j*hatI.channels() + 3];
		}
	}
	cvtColor(hatI, hatI, CV_RGBA2RGB);

	//Mat src_mask = 255 * Mat::ones(hatI.rows, hatI.cols, hatI.depth());
	threshold(hatDist, hatDist, 1, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

	//copyMakeBorder(hatDist, hatDist, 10, 10, 10, 10, BORDER_CONSTANT, 0);
	//copyMakeBorder(maskEyes, maskEyes, 2, 2, 2, 2, BORDER_CONSTANT, 0);

	int erosion_size = 7;
	//dilation
	Mat element = getStructuringElement(MORPH_ELLIPSE,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));
	dilate(hatDist, hatDist, element);
#ifdef _DEBUG
	imshow("hatMask", hatDist);
	waitKey(1);
#endif
	int x = faceBox.x + hatI.cols / 2 - (hatI.cols - faceBox.width) / 2 + hatI.cols*hats[hatNumber]->xoffset;
	int y = faceBox.y - hatI.rows / 2 - (hatI.rows - faceBox.height) / 2 + hatI.rows*hats[hatNumber]->yoffset;
	cv::Point center(x, y);
	
	seamlessClone(hatI, *img, hatDist, center, *img, NORMAL_CLONE);
	/*
	distanceTransform(hatDist, hatDist, CV_DIST_C, 3);
	normalize(hatDist, hatDist, 0, 1., NORM_MINMAX);
#ifdef _DEBUG
	imshow("hatDistance", hatDist);
	waitKey(1);
#endif

	int hstart = faceBox.y - hatI.rows + hatI.rows*hats[hatNumber]->yoffset;
	int wstart = faceBox.x - (hatI.cols - faceBox.width) / 2 + hatI.cols*hats[hatNumber]->xoffset;
	int hstartDiff = 0;
	if (hstart <0)
	{
		hstartDiff = 0 - hstart;
	}

	for (int i = hstart + hstartDiff, ii = hstartDiff; ii<hatI.rows; i++, ii++)
	{
		for (int j = wstart, jj = 0; jj<hatI.cols; j++, jj++)
		{
			if (hatI.data[(ii*hatI.cols + jj)*hatI.channels() + 3] >0)
			{
				img->data[(i*img->cols + j) * 3] = hatI.data[(ii*hatI.cols + jj)*hatI.channels()];
				img->data[(i*img->cols + j) * 3 + 1] = hatI.data[(ii*hatI.cols + jj)*hatI.channels() + 1];
				img->data[(i*img->cols + j) * 3 + 2] = hatI.data[(ii*hatI.cols + jj)*hatI.channels() + 2];
			}

		}
	}
	*/
}


void hajiFirooz::addHat2(Mat *img, cv::Rect faceBox,int hatNumber){

	double headWidth = (double)faceBox.width*hats[hatNumber]->headHatRation;
	hatI = hats[hatNumber]->hatImg.clone();
	
	
	double ratio = (double)headWidth/(double)hatI.cols;
	resize(hatI,hatI,cv::Size(),ratio,ratio);
	cvtColor(hatI,hatDist,CV_RGBA2GRAY);
	for (int i = 0 ; i<hatI.rows;i++ )
	{
		for (int j = 0; j<hatI.cols;j++)
		{
			hatDist.data[i*hatDist.step+j*hatDist.channels()] = hatI.data[i*hatI.step+j*hatI.channels()+3];
		}
	}
	
	threshold(hatDist,hatDist, 1, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	distanceTransform(hatDist,hatDist,CV_DIST_C,3);
	normalize(hatDist, hatDist, 0, 1., NORM_MINMAX);
#ifdef _DEBUG
	imshow("hatDistance",hatDist);
	waitKey(1);
#endif

	int hstart = faceBox.y-hatI.rows +hatI.rows*hats[hatNumber]->yoffset;
	int wstart = faceBox.x -(hatI.cols-faceBox.width)/2 + hatI.cols*hats[hatNumber]->xoffset;
	int hstartDiff = 0;
	if (hstart <0)
	{
		hstartDiff  = 0 - hstart;
	}

	for (int i=hstart+hstartDiff, ii=hstartDiff;ii<hatI.rows;i++,ii++)
	{
		for (int j = wstart , jj=0;jj<hatI.cols;j++,jj++)
		{
			if (hatI.data[(ii*hatI.cols+jj)*hatI.channels()+3 ] >0)
			{
				img->data[(i*img->cols+j)*3] = hatI.data[(ii*hatI.cols+jj)*hatI.channels()];
				img->data[(i*img->cols+j)*3+1] = hatI.data[(ii*hatI.cols+jj)*hatI.channels()+1];
				img->data[(i*img->cols+j)*3+2] = hatI.data[(ii*hatI.cols+jj)*hatI.channels()+2];
			}

		}
	}

}


void hajiFirooz::addHat(Mat *img, cv::Rect faceBox,int hatNumber){
	
	double headWidth = (double)faceBox.width*hats[hatNumber]->headHatRation;
	hatI = hats[hatNumber]->hatImg.clone();
	double ratio = (double)headWidth/(double)hatI.cols;
	resize(hatI,hatI,cv::Size(),ratio,ratio);
	int hstart = faceBox.y-hatI.rows +hatI.rows*hats[hatNumber]->yoffset;
	int wstart = faceBox.x -(hatI.cols-faceBox.width)/2 + hatI.cols*hats[hatNumber]->xoffset;
	int hstartDiff = 0;
	if (hstart <0)
	{
		hstartDiff  = 0- hstart;
	}
	
	for (int i=hstart+hstartDiff, ii=hstartDiff;ii<hatI.rows;i++,ii++)
	{
		for (int j = wstart , jj=0;jj<hatI.cols;j++,jj++)
		{
			if (hatI.data[(ii*hatI.cols+jj)*hatI.channels()+3 ] >50)
			{
				img->data[(i*img->cols+j)*3] = hatI.data[(ii*hatI.cols+jj)*hatI.channels()];
				img->data[(i*img->cols+j)*3+1] = hatI.data[(ii*hatI.cols+jj)*hatI.channels()+1];
				img->data[(i*img->cols+j)*3+2] = hatI.data[(ii*hatI.cols+jj)*hatI.channels()+2];
			}
			
		}
	}

}


bool hajiFirooz::makeFirooz(Mat imgscr,Mat imgdes,int faceNumbers,int hatType, int darknessType){
	int runTime;
	return makeFirooz(imgscr,imgdes,faceNumbers,hatType,darknessType,&runTime); 
}
bool hajiFirooz::makeFirooz(Mat imgscr,Mat imgdes,int faceNumbers,int hatType, int darknessType,int *runtime){
	//static const char* const path = "../data/0.jpg";
	
	try{
	
		cvtColor(imgscr,img,CV_RGB2GRAY);
		*runtime= timeGetTime();
		//Mat im2 = imgscr.clone();
	
		// imghsv = im2.clone();
		//cvtColor(imgscr,imghsv,CV_RGB2HSV);

		if (!img.data)
		{
			throw("Cannot load the image \n");
			//exit(1);
		}

		int foundface;
		float landmarks[2 * stasm_NLANDMARKS]; // x,y coords (note the 2)

		if (!stasm_search_single(&foundface, landmarks,
			(const char*)img.data, img.cols, img.rows, path, "data"))
		{
			throw ("Error in stasm: %s \n",stasm_lasterr());
			//exit(1);
		}

		if (!foundface)
			throw("No face found in %s\n", path);
		else
		{
			// draw the landmarks on the image as white dots (image is monochrome)
			//stasm_force_points_into_image(landmarks, img.cols, img.rows);
			//for (int i = 0; i < stasm_NLANDMARKS; i++)
				//img(cvRound(landmarks[i*2+1]), cvRound(landmarks[i*2])) = 255;
			faceRect=makeFaceBox(landmarks);
			cv::Point pFace(faceRect.x,faceRect.y);
			//cv::rectangle(im2,faceRect,Scalar(255,0,0),2);

			//maybe it needed to be freed at the end of each run *******************************
			effectMask = Mat(faceRect.height,faceRect.width,CV_8U,0.0);

#ifdef _DEBUG
			for (int i = 0; i < 15; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}

#endif
			ROI_Poly.clear();
			ROI_Vertices.clear();
			for (int i = 0; i < 16; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 100, 8,0);                 
#ifdef _DEBUG
			for (int i = 16; i < 21; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif

			ROI_Poly.clear();
			ROI_Vertices.clear();
			for (int i = 16; i < 22; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);                 

#ifdef _DEBUG
			for (int i = 22; i <27 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif

			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 22; i < 28; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);

			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);   

#ifdef _DEBUG
			for (int i = 30; i <38 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif


			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 30; i < 38; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  

#ifdef _DEBUG
			for (int i = 39; i <47 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif

			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 40; i < 47; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  

			/*
			//nose
			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 48; i < 55; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  
			*/
#ifdef _DEBUG
			for (int i = 59; i <76 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif
			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 72; i < 77; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			for (int i = 59; i < 66; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  

			
	
			dilate(effectMask, effectMask, dielement );

			makeFiroozDistRGB(&imgdes,&effectMask,faceRect,darknessType);
			addHat3(&imgdes,faceRect,hatType-1);
/*			
			//cvtColor(imghsv,img,CV_HSV2RGB);
			if (hatType==1)
			{
				addHat(&imgdes,&hat1,faceRect);
			}
			else if (hatType==2)
			{
				addHat(&imgdes,&hat2,faceRect);
			}
			else if (hatType==3)
			{
				addHat(&imgdes,&hat3,faceRect);
			}
*/			
			
#ifdef _DEBUG
			imshow("MASKDEBUG",effectMask);
#endif
		}

		*runtime = *runtime - timeGetTime();
#ifdef _DEBUG
		cv::imshow("desImageDEUG", imgdes);
		cv::imshow("sourceIMGDebug", imgscr);	
		cv::imshow("ASMDEBUG", img);	
		cv::waitKey(1);
#endif
		return 1;
	}
	catch(std::exception e) {
		e.what();
		return 0;
	}
	

}