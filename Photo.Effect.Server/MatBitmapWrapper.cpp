#include "stdafx.h"
#include "MatBitmapWrapper.h"

using namespace PhotoServer;
using namespace System::Drawing;
using namespace System::Drawing::Imaging;
MatBitmapWrapper::MatBitmapWrapper()
{
}

System::Drawing::Bitmap ^ MatBitmapWrapper::Mat2Bmp(cv::Mat matImg){
	/*
	cv::Size size = matImg.size();
	
	Bitmap ^bitmap = gcnew Bitmap(size.width, size.height, matImg.step1(), System::Drawing::Imaging::PixelFormat::Format24bppRgb,  System::IntPtr((void *) matImg.data) );
	return bitmap;
	*/
	if (matImg.type() != CV_8UC3)
	{
		throw gcnew NotSupportedException("Only images of type CV_8UC3 are supported for conversion to Bitmap");
	}

	//create the bitmap and get the pointer to the data
	PixelFormat fmt(PixelFormat::Format24bppRgb);
	Bitmap ^bmpimg = gcnew Bitmap(matImg.cols, matImg.rows, fmt);

	BitmapData ^data = bmpimg->LockBits(System::Drawing::Rectangle(0, 0, matImg.cols, matImg.rows), ImageLockMode::WriteOnly, fmt);
	
	Byte *dstData = reinterpret_cast<Byte*>(data->Scan0.ToPointer());

	unsigned char *srcData = matImg.data;

	for (int row = 0; row < data->Height; ++row)
	{
		memcpy(reinterpret_cast<void*>(&dstData[row*data->Stride]), reinterpret_cast<void*>(&srcData[row*matImg.step]), matImg.cols*matImg.channels());
	}

	bmpimg->UnlockBits(data);

	return bmpimg;
}
cv::Mat MatBitmapWrapper::Bmp2Mat(Bitmap ^bmpImg){
	System::Drawing::Rectangle blank = System::Drawing::Rectangle(0, 0, bmpImg->Width, bmpImg->Height);

	System::Drawing::Imaging::BitmapData^ bmpdata = bmpImg->LockBits(blank, System::Drawing::Imaging::ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
	cv::Mat matImg(cv::Size(bmpImg->Width, bmpImg->Height), CV_8UC3,(void*) bmpdata->Scan0.ToPointer(),bmpdata->Stride);
	bmpImg->UnlockBits(bmpdata);
	return matImg;

}