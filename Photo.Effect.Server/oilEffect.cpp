#include "stdafx.h"
#include "oilEffect.h"


oilEffect::oilEffect()
{
}


oilEffect::~oilEffect()
{
}


void oilEffect::Process(Mat *pbyDataIn_i,
	const int nRadius_i,
	const float fIntensityLevels_i,
	Mat* pbyDataOut_o)
{
	int nIntensityCount[256];
	int nSumR[256];
	int nSumG[256];
	int nSumB[256];

	// Border pixes( depends on nRadius) will become black.
	// On increasing radius boundary pixels should set as black.
	//memset(pbyDataOut_o, 0, pbyDataIn_i->cols * pbyDataIn_i->rows * 3);

	// If total bytes in a row of image is not divisible by four, 
	// blank bytes will be padded to the end of the row.
	// nBytesInARow bytes are the actual size of a row instead of nWidth * 3.
	// If width is 9, then actual bytes in a row will will be 28, and not 27.
	int nBytesInARow = ceil(pbyDataIn_i->cols * 3 / 4.0) * 4.0;

	// nRadius pixels are avoided from left, right top, and bottom edges.
	for (int nY = nRadius_i; nY < pbyDataIn_i->rows - nRadius_i; nY++)
	{
		for (int nX = nRadius_i; nX < pbyDataIn_i->cols - nRadius_i; nX++)
		{
			// Reset calculations of last pixel.
			memset(nIntensityCount, 0, sizeof(nIntensityCount));
			memset(nSumR, 0, sizeof(nSumR));
			memset(nSumG, 0, sizeof(nSumG));
			memset(nSumB, 0, sizeof(nSumB));

			// Find intensities of nearest nRadius pixels in four direction.
			for (int nY_O = -nRadius_i; nY_O <= nRadius_i; nY_O++)
			{
				for (int nX_O = -nRadius_i; nX_O <= nRadius_i; nX_O++)
				{
					int nR = pbyDataIn_i->data[(nX + nX_O) * 3 + (nY + nY_O) * nBytesInARow];
					int nG = pbyDataIn_i->data[(nX + nX_O) * 3 + (nY + nY_O) * nBytesInARow + 1];
					int nB = pbyDataIn_i->data[(nX + nX_O) * 3 + (nY + nY_O) * nBytesInARow + 2];

					// Find intensity of RGB value and apply intensity level.
					int nCurIntensity = (((nR + nG + nB) / 3.0) * fIntensityLevels_i) / 255;
					if (nCurIntensity > 255)
						nCurIntensity = 255;
					int i = nCurIntensity;
					nIntensityCount[i]++;

					nSumR[i] = nSumR[i] + nR;
					nSumG[i] = nSumG[i] + nG;
					nSumB[i] = nSumB[i] + nB;
				}
			}

			int nOutR = 0;
			int nOutG = 0;
			int nOutB = 0;

			int nCurMax = 0;
			int nMaxIndex = 0;
			for (int nI = 0; nI < 256; nI++)
			{
				if (nIntensityCount[nI] > nCurMax)
				{
					nCurMax = nIntensityCount[nI];
					nMaxIndex = nI;
				}
			}

			nOutR = nSumR[nMaxIndex] / nCurMax;
			nOutG = nSumG[nMaxIndex] / nCurMax;
			nOutB = nSumB[nMaxIndex] / nCurMax;

			pbyDataOut_o->data[(nX)* 3 + (nY)* nBytesInARow] = nOutR;
			pbyDataOut_o->data[(nX)* 3 + (nY)* nBytesInARow + 1] = nOutG;
			pbyDataOut_o->data[(nX)* 3 + (nY)* nBytesInARow + 2] = nOutB;
		}
	}
}