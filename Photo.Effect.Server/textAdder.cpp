#include "StdAfx.h"
#include "textAdder.h"

using namespace PhotoServer;
textAdder::textAdder(void)
{
	fontName = gcnew System::String(L"IranNastaliq.ttf");

	System::Drawing::Text::PrivateFontCollection ^pfcoll = gcnew  System::Drawing::Text::PrivateFontCollection();
	//put a font file under a Fonts directory within your application root

	//pfcoll->AddFontFile(L"~/Fonts/" + fontName);
	pfcoll->AddFontFile( fontName);
	//pfcoll->Families
	ff =  (pfcoll->Families[0]);
	
}

bool textAdder::addText(Bitmap ^bitmap,System::String ^text,PointF position,int fontSize){
	return addText(bitmap,text,position,fontSize,gcnew SolidBrush(Color::Black));
}

bool textAdder::addText(Bitmap ^bitmap,String ^text,PointF position, int fontSize ,  Brush ^brush){
	
	try{
		
		
		System::Drawing::Font ^font = gcnew System::Drawing::Font(ff, fontSize, FontStyle::Bold);
		System::Drawing::Graphics ^graphics = System::Drawing::Graphics::FromImage(bitmap);		
		graphics->DrawString(text, font, brush, position);
		
		return true;
	}
	catch (System::Exception ^e){
		return false;
	}
}
