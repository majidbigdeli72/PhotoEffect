#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "opencv/highgui.h"
#include "stasm_lib.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include <Windows.h>
#define makeFaceBox(landmarks) cv::Rect(landmarks[0],landmarks[14*2+1],landmarks[11*2]-landmarks[0],landmarks[6*2+1]-landmarks[14*2+1]);

using namespace cv;
using namespace std;

struct flagSpec{
	double Ratio;
	double xoffset, yoffset;
	string filePath;
	Mat flagImg;
};
class flagger
{
private:
	void loadFlags(string XMLFilePath, flagSpec *flagData);
	void addFlag3(Mat *img, cv::Rect flagBox, int flagNumber);
	void addFlag2(Mat *img, cv::Rect flagBox, int hatNumber);
	void addFlag(Mat *img, cv::Rect faceBox, int hatNumber);

	vector<cv::Point> ROI_Poly;
	vector<cv::Point> ROI_Vertices;

	vector<flagSpec*> flags;
	//Mat maskFace,maskEyes,maskTotal;
	Mat effectMask,img;
	cv::Rect faceRect;
	Mat flagI, flagDist;
	//Mat hatDist;
public:
	bool test(Mat imgscr,Mat *imgdes,int faceNumbers);
	flagger(int flgNumbers);
	~flagger(void);
};

