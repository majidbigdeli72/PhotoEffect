#pragma once
namespace PhotoServer {
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Runtime::InteropServices;

	public ref class textAdder
	{
	private:
		System::String ^fontName;
		FontFamily ^ff;

	public:
		textAdder(void);
		bool addText(Bitmap ^bitmap, System::String ^text, PointF position, int fontSize, Brush ^brush);
		bool addText(Bitmap ^bitmap, System::String ^text, PointF position, int fontSize);

	};

}