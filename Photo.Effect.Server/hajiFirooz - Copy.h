#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "opencv/highgui.h"
#include "stasm_lib.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include <Windows.h>

using namespace cv;
#define makeFaceBox(landmarks) cv::Rect(landmarks[0],landmarks[14*2+1],landmarks[11*2]-landmarks[0],landmarks[6*2+1]-landmarks[14*2+1]);
#define darkingLevel 100
#define headHatRatio 1.5


ref class hajiFirooz
{
public:
	Mat *im;
	hajiFirooz(void);
	bool makeFirooz(Mat imgscr,Mat imgdes,int faceNumbers,int *runtime); //faceNumbers  1: single face   2: multiface
private:

	void makeFiroozDistRGB(Mat *img,Mat *mask, cv::Rect faceBox);
	void addHat(Mat *img,Mat* hat , cv::Rect faceBox);
};

