 #pragma once
#include <opencv2\opencv.hpp>
namespace PhotoServer {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	public ref class MatBitmapWrapper
	{
	public:
		Bitmap ^ Mat2Bmp(cv::Mat matImg);
		cv::Mat Bmp2Mat(Bitmap ^bmpImg);
		MatBitmapWrapper();
	};

}