
#pragma once
#include "MatBitmapWrapper.h"


namespace PhotoServer {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Drawing;
	public ref class webAPIInterface
	{
	private:
		MatBitmapWrapper ^mat2Bmp;
	public:
		Bitmap ^cartoofify(Bitmap ^inp, int level);
		webAPIInterface();
	};

}