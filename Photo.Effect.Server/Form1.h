﻿#pragma once
#include <opencv2/opencv.hpp>
#include <msclr\marshal_cppstd.h>
#include "textAdder.h"

#pragma comment(lib,"Winmm.lib")
/*
#ifdef _DEBUG
#pragma comment (lib,"opencv_world310d.lib")
#else
#pragma comment (lib,"opencv_world310.lib")
#endif
*/
#ifdef _DEBUG
#pragma comment (lib,"opencv_calib3d310d.lib")
#pragma comment (lib,"opencv_core310d.lib")
#pragma comment (lib,"opencv_features2d310d.lib")
#pragma comment (lib,"opencv_flann310d.lib")
#pragma comment (lib,"opencv_highgui310d.lib")
#pragma comment (lib,"opencv_imgcodecs310d.lib")
#pragma comment (lib,"opencv_imgproc310d.lib")
#pragma comment (lib,"opencv_ml310d.lib")
#pragma comment (lib,"opencv_objdetect310d.lib")
#pragma comment (lib,"opencv_photo310d.lib")
#pragma comment (lib,"opencv_shape310d.lib")
#pragma comment (lib,"opencv_stitching310d.lib")
#pragma comment (lib,"opencv_superres310d.lib")
#pragma comment (lib,"opencv_ts310d.lib")
#pragma comment (lib,"opencv_video310d.lib")
#pragma comment (lib,"opencv_videoio310d.lib")
#pragma comment (lib,"opencv_videostab310d.lib")
#else
#pragma comment (lib,"opencv_calib3d310.lib")
#pragma comment (lib,"opencv_core310.lib")
#pragma comment (lib,"opencv_features2d310.lib")
#pragma comment (lib,"opencv_flann310.lib")
#pragma comment (lib,"opencv_highgui310.lib")
#pragma comment (lib,"opencv_imgcodecs310.lib")
#pragma comment (lib,"opencv_imgproc310.lib")
#pragma comment (lib,"opencv_ml310.lib")
#pragma comment (lib,"opencv_objdetect310.lib")
#pragma comment (lib,"opencv_photo310.lib")
#pragma comment (lib,"opencv_shape310.lib")
#pragma comment (lib,"opencv_stitching310.lib")
#pragma comment (lib,"opencv_superres310.lib")
//#pragma comment (lib,"opencv_ts310.lib")
#pragma comment (lib,"opencv_video310.lib")
#pragma comment (lib,"opencv_videoio310.lib")
#pragma comment (lib,"opencv_videostab310.lib")
#endif
#include "photoShopBlends.h"
#include "hajiFirooz.h"
#include "oilEffect.h"
#include "flagger.h"
#include "MatBitmapWrapper.h"
#include "webAPIInterface.h"
hajiFirooz *hajiMaker;
namespace PhotoServer {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;
	private: System::Windows::Forms::Button^  buttonTest;
	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  tabPageMain;
	private: System::Windows::Forms::TabPage^  tabPageTest;
	private: System::Windows::Forms::GroupBox^  groupBoxFirooz;
	private: System::Windows::Forms::GroupBox^  groupBoxFiroozHat;
	private: System::Windows::Forms::RadioButton^  radioButtonHat3;
	private: System::Windows::Forms::RadioButton^  radioButtonHat2;
	private: System::Windows::Forms::RadioButton^  radioButtonHat1;
	private: System::Windows::Forms::GroupBox^  groupBoxFiroozDarkness;
	private: System::Windows::Forms::RadioButton^  radioButtonFiroozDarkLow;
	private: System::Windows::Forms::RadioButton^  radioButtonFiroozDarkMedium;
	private: System::Windows::Forms::RadioButton^  radioButtonFiroozDarkHigh;
	private: System::Windows::Forms::Label^  labelFilePath;
	private: System::Windows::Forms::TextBox^  textBoxImagePath;


	private: System::Windows::Forms::Button^  buttonLoadTestImage;
	private: System::Windows::Forms::OpenFileDialog^  OFDTestImage;
	private: System::Windows::Forms::Button^  buttonFiroozTest;
	private: System::Windows::Forms::GroupBox^  groupBoxTextAdder;
	private: System::Windows::Forms::TextBox^  textBoxAdderText;

	private: System::Windows::Forms::Label^  labelTextAdder;
	private: System::Windows::Forms::TextBox^  textBoxAdderFontSize;
	private: System::Windows::Forms::TextBox^  textBoxAdderX;
	private: System::Windows::Forms::TextBox^  textBoxAdderY;



	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  buttonAdderText;
	private: System::Windows::Forms::PictureBox^  pictureBoxTest;
	private: System::Windows::Forms::TextBox^  textBoxAdderOut;
	private: System::Windows::Forms::GroupBox^  groupBoxCartoon;
	private: System::Windows::Forms::Button^  buttonCartoonMaker;
	private: System::Windows::Forms::GroupBox^  groupBoxPencil;
	private: System::Windows::Forms::GroupBox^  groupBoxCanny;
	private: System::Windows::Forms::Button^  buttonCanny;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  buttonFlagger;
	private: System::Windows::Forms::Button^  buttonPencil;






			 
#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->buttonTest = (gcnew System::Windows::Forms::Button());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPageMain = (gcnew System::Windows::Forms::TabPage());
			this->tabPageTest = (gcnew System::Windows::Forms::TabPage());
			this->buttonFlagger = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->groupBoxCanny = (gcnew System::Windows::Forms::GroupBox());
			this->buttonCanny = (gcnew System::Windows::Forms::Button());
			this->groupBoxPencil = (gcnew System::Windows::Forms::GroupBox());
			this->buttonPencil = (gcnew System::Windows::Forms::Button());
			this->groupBoxCartoon = (gcnew System::Windows::Forms::GroupBox());
			this->buttonCartoonMaker = (gcnew System::Windows::Forms::Button());
			this->pictureBoxTest = (gcnew System::Windows::Forms::PictureBox());
			this->groupBoxTextAdder = (gcnew System::Windows::Forms::GroupBox());
			this->textBoxAdderOut = (gcnew System::Windows::Forms::TextBox());
			this->buttonAdderText = (gcnew System::Windows::Forms::Button());
			this->textBoxAdderFontSize = (gcnew System::Windows::Forms::TextBox());
			this->textBoxAdderX = (gcnew System::Windows::Forms::TextBox());
			this->textBoxAdderY = (gcnew System::Windows::Forms::TextBox());
			this->textBoxAdderText = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->labelTextAdder = (gcnew System::Windows::Forms::Label());
			this->buttonLoadTestImage = (gcnew System::Windows::Forms::Button());
			this->labelFilePath = (gcnew System::Windows::Forms::Label());
			this->textBoxImagePath = (gcnew System::Windows::Forms::TextBox());
			this->groupBoxFirooz = (gcnew System::Windows::Forms::GroupBox());
			this->buttonFiroozTest = (gcnew System::Windows::Forms::Button());
			this->groupBoxFiroozDarkness = (gcnew System::Windows::Forms::GroupBox());
			this->radioButtonFiroozDarkLow = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonFiroozDarkMedium = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonFiroozDarkHigh = (gcnew System::Windows::Forms::RadioButton());
			this->groupBoxFiroozHat = (gcnew System::Windows::Forms::GroupBox());
			this->radioButtonHat3 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonHat2 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonHat1 = (gcnew System::Windows::Forms::RadioButton());
			this->OFDTestImage = (gcnew System::Windows::Forms::OpenFileDialog());
			this->tabControl1->SuspendLayout();
			this->tabPageTest->SuspendLayout();
			this->groupBoxCanny->SuspendLayout();
			this->groupBoxPencil->SuspendLayout();
			this->groupBoxCartoon->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxTest))->BeginInit();
			this->groupBoxTextAdder->SuspendLayout();
			this->groupBoxFirooz->SuspendLayout();
			this->groupBoxFiroozDarkness->SuspendLayout();
			this->groupBoxFiroozHat->SuspendLayout();
			this->SuspendLayout();
			// 
			// buttonTest
			// 
			this->buttonTest->Location = System::Drawing::Point(39, 500);
			this->buttonTest->Name = L"buttonTest";
			this->buttonTest->Size = System::Drawing::Size(75, 23);
			this->buttonTest->TabIndex = 0;
			this->buttonTest->Text = L"button1";
			this->buttonTest->UseVisualStyleBackColor = true;
			this->buttonTest->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPageMain);
			this->tabControl1->Controls->Add(this->tabPageTest);
			this->tabControl1->Location = System::Drawing::Point(162, 12);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(746, 515);
			this->tabControl1->TabIndex = 2;
			// 
			// tabPageMain
			// 
			this->tabPageMain->Location = System::Drawing::Point(4, 25);
			this->tabPageMain->Name = L"tabPageMain";
			this->tabPageMain->Padding = System::Windows::Forms::Padding(3);
			this->tabPageMain->Size = System::Drawing::Size(738, 486);
			this->tabPageMain->TabIndex = 0;
			this->tabPageMain->Text = L"Robot Controller";
			this->tabPageMain->UseVisualStyleBackColor = true;
			// 
			// tabPageTest
			// 
			this->tabPageTest->Controls->Add(this->buttonFlagger);
			this->tabPageTest->Controls->Add(this->button1);
			this->tabPageTest->Controls->Add(this->groupBoxCanny);
			this->tabPageTest->Controls->Add(this->groupBoxPencil);
			this->tabPageTest->Controls->Add(this->groupBoxCartoon);
			this->tabPageTest->Controls->Add(this->pictureBoxTest);
			this->tabPageTest->Controls->Add(this->groupBoxTextAdder);
			this->tabPageTest->Controls->Add(this->buttonLoadTestImage);
			this->tabPageTest->Controls->Add(this->labelFilePath);
			this->tabPageTest->Controls->Add(this->textBoxImagePath);
			this->tabPageTest->Controls->Add(this->groupBoxFirooz);
			this->tabPageTest->Location = System::Drawing::Point(4, 25);
			this->tabPageTest->Name = L"tabPageTest";
			this->tabPageTest->Padding = System::Windows::Forms::Padding(3);
			this->tabPageTest->Size = System::Drawing::Size(738, 486);
			this->tabPageTest->TabIndex = 1;
			this->tabPageTest->Text = L"Test ";
			this->tabPageTest->UseVisualStyleBackColor = true;
			// 
			// buttonFlagger
			// 
			this->buttonFlagger->Location = System::Drawing::Point(13, 341);
			this->buttonFlagger->Name = L"buttonFlagger";
			this->buttonFlagger->Size = System::Drawing::Size(90, 30);
			this->buttonFlagger->TabIndex = 10;
			this->buttonFlagger->Text = L"Flagger";
			this->buttonFlagger->UseVisualStyleBackColor = true;
			this->buttonFlagger->Click += gcnew System::EventHandler(this, &Form1::buttonFlagger_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(426, 278);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 34);
			this->button1->TabIndex = 9;
			this->button1->Text = L"oil";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click_1);
			// 
			// groupBoxCanny
			// 
			this->groupBoxCanny->Controls->Add(this->buttonCanny);
			this->groupBoxCanny->Location = System::Drawing::Point(279, 260);
			this->groupBoxCanny->Name = L"groupBoxCanny";
			this->groupBoxCanny->Size = System::Drawing::Size(130, 60);
			this->groupBoxCanny->TabIndex = 8;
			this->groupBoxCanny->TabStop = false;
			this->groupBoxCanny->Text = L"Canny sketch";
			// 
			// buttonCanny
			// 
			this->buttonCanny->Location = System::Drawing::Point(6, 21);
			this->buttonCanny->Name = L"buttonCanny";
			this->buttonCanny->Size = System::Drawing::Size(75, 29);
			this->buttonCanny->TabIndex = 0;
			this->buttonCanny->Text = L"Canny";
			this->buttonCanny->UseVisualStyleBackColor = true;
			this->buttonCanny->Click += gcnew System::EventHandler(this, &Form1::buttonCanny_Click);
			// 
			// groupBoxPencil
			// 
			this->groupBoxPencil->Controls->Add(this->buttonPencil);
			this->groupBoxPencil->Location = System::Drawing::Point(137, 260);
			this->groupBoxPencil->Name = L"groupBoxPencil";
			this->groupBoxPencil->Size = System::Drawing::Size(136, 60);
			this->groupBoxPencil->TabIndex = 7;
			this->groupBoxPencil->TabStop = false;
			this->groupBoxPencil->Text = L"Pencil";
			// 
			// buttonPencil
			// 
			this->buttonPencil->Location = System::Drawing::Point(6, 21);
			this->buttonPencil->Name = L"buttonPencil";
			this->buttonPencil->Size = System::Drawing::Size(75, 33);
			this->buttonPencil->TabIndex = 0;
			this->buttonPencil->Text = L"Pencil it";
			this->buttonPencil->UseVisualStyleBackColor = true;
			this->buttonPencil->Click += gcnew System::EventHandler(this, &Form1::buttonPencil_Click);
			// 
			// groupBoxCartoon
			// 
			this->groupBoxCartoon->Controls->Add(this->buttonCartoonMaker);
			this->groupBoxCartoon->Location = System::Drawing::Point(7, 259);
			this->groupBoxCartoon->Name = L"groupBoxCartoon";
			this->groupBoxCartoon->Size = System::Drawing::Size(124, 61);
			this->groupBoxCartoon->TabIndex = 6;
			this->groupBoxCartoon->TabStop = false;
			this->groupBoxCartoon->Text = L"Cartoon";
			// 
			// buttonCartoonMaker
			// 
			this->buttonCartoonMaker->Location = System::Drawing::Point(6, 26);
			this->buttonCartoonMaker->Name = L"buttonCartoonMaker";
			this->buttonCartoonMaker->Size = System::Drawing::Size(75, 29);
			this->buttonCartoonMaker->TabIndex = 0;
			this->buttonCartoonMaker->Text = L"Make it Cartoon";
			this->buttonCartoonMaker->UseVisualStyleBackColor = true;
			this->buttonCartoonMaker->Click += gcnew System::EventHandler(this, &Form1::buttonCartoonMaker_Click);
			// 
			// pictureBoxTest
			// 
			this->pictureBoxTest->Location = System::Drawing::Point(471, 6);
			this->pictureBoxTest->Name = L"pictureBoxTest";
			this->pictureBoxTest->Size = System::Drawing::Size(261, 225);
			this->pictureBoxTest->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBoxTest->TabIndex = 5;
			this->pictureBoxTest->TabStop = false;
			// 
			// groupBoxTextAdder
			// 
			this->groupBoxTextAdder->Controls->Add(this->textBoxAdderOut);
			this->groupBoxTextAdder->Controls->Add(this->buttonAdderText);
			this->groupBoxTextAdder->Controls->Add(this->textBoxAdderFontSize);
			this->groupBoxTextAdder->Controls->Add(this->textBoxAdderX);
			this->groupBoxTextAdder->Controls->Add(this->textBoxAdderY);
			this->groupBoxTextAdder->Controls->Add(this->textBoxAdderText);
			this->groupBoxTextAdder->Controls->Add(this->label3);
			this->groupBoxTextAdder->Controls->Add(this->label2);
			this->groupBoxTextAdder->Controls->Add(this->label1);
			this->groupBoxTextAdder->Controls->Add(this->labelTextAdder);
			this->groupBoxTextAdder->Location = System::Drawing::Point(6, 145);
			this->groupBoxTextAdder->Name = L"groupBoxTextAdder";
			this->groupBoxTextAdder->Size = System::Drawing::Size(459, 109);
			this->groupBoxTextAdder->TabIndex = 4;
			this->groupBoxTextAdder->TabStop = false;
			this->groupBoxTextAdder->Text = L"Text Adder";
			// 
			// textBoxAdderOut
			// 
			this->textBoxAdderOut->Location = System::Drawing::Point(248, 81);
			this->textBoxAdderOut->Name = L"textBoxAdderOut";
			this->textBoxAdderOut->Size = System::Drawing::Size(201, 22);
			this->textBoxAdderOut->TabIndex = 4;
			this->textBoxAdderOut->Text = L"data\\textOut.jpg";
			// 
			// buttonAdderText
			// 
			this->buttonAdderText->Location = System::Drawing::Point(6, 41);
			this->buttonAdderText->Name = L"buttonAdderText";
			this->buttonAdderText->Size = System::Drawing::Size(91, 31);
			this->buttonAdderText->TabIndex = 3;
			this->buttonAdderText->Text = L"Add Text";
			this->buttonAdderText->UseVisualStyleBackColor = true;
			this->buttonAdderText->Click += gcnew System::EventHandler(this, &Form1::buttonAdderText_Click);
			// 
			// textBoxAdderFontSize
			// 
			this->textBoxAdderFontSize->Location = System::Drawing::Point(263, 51);
			this->textBoxAdderFontSize->Name = L"textBoxAdderFontSize";
			this->textBoxAdderFontSize->Size = System::Drawing::Size(35, 22);
			this->textBoxAdderFontSize->TabIndex = 0;
			this->textBoxAdderFontSize->Text = L"50";
			this->textBoxAdderFontSize->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBoxAdderFontSize->TextChanged += gcnew System::EventHandler(this, &Form1::textBoxTextAdderText_TextChanged);
			// 
			// textBoxAdderX
			// 
			this->textBoxAdderX->Location = System::Drawing::Point(330, 49);
			this->textBoxAdderX->Name = L"textBoxAdderX";
			this->textBoxAdderX->Size = System::Drawing::Size(46, 22);
			this->textBoxAdderX->TabIndex = 0;
			this->textBoxAdderX->Text = L"10";
			this->textBoxAdderX->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBoxAdderX->TextChanged += gcnew System::EventHandler(this, &Form1::textBoxTextAdderText_TextChanged);
			// 
			// textBoxAdderY
			// 
			this->textBoxAdderY->Location = System::Drawing::Point(407, 50);
			this->textBoxAdderY->Name = L"textBoxAdderY";
			this->textBoxAdderY->Size = System::Drawing::Size(43, 22);
			this->textBoxAdderY->TabIndex = 0;
			this->textBoxAdderY->Text = L"10";
			this->textBoxAdderY->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBoxAdderY->TextChanged += gcnew System::EventHandler(this, &Form1::textBoxTextAdderText_TextChanged);
			// 
			// textBoxAdderText
			// 
			this->textBoxAdderText->Location = System::Drawing::Point(248, 21);
			this->textBoxAdderText->Name = L"textBoxAdderText";
			this->textBoxAdderText->Size = System::Drawing::Size(201, 22);
			this->textBoxAdderText->TabIndex = 0;
			this->textBoxAdderText->Text = L"عیدت مبارک";
			this->textBoxAdderText->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->textBoxAdderText->TextChanged += gcnew System::EventHandler(this, &Form1::textBoxTextAdderText_TextChanged);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(382, 52);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(21, 17);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Y:";
			this->label3->Click += gcnew System::EventHandler(this, &Form1::labelTextAdder_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(308, 51);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(21, 17);
			this->label2->TabIndex = 2;
			this->label2->Text = L"X:";
			this->label2->Click += gcnew System::EventHandler(this, &Form1::labelTextAdder_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(197, 53);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(67, 17);
			this->label1->TabIndex = 2;
			this->label1->Text = L"FontSize:";
			this->label1->Click += gcnew System::EventHandler(this, &Form1::labelTextAdder_Click);
			// 
			// labelTextAdder
			// 
			this->labelTextAdder->AutoSize = true;
			this->labelTextAdder->Location = System::Drawing::Point(212, 23);
			this->labelTextAdder->Name = L"labelTextAdder";
			this->labelTextAdder->Size = System::Drawing::Size(39, 17);
			this->labelTextAdder->TabIndex = 2;
			this->labelTextAdder->Text = L"Text:";
			this->labelTextAdder->Click += gcnew System::EventHandler(this, &Form1::labelTextAdder_Click);
			// 
			// buttonLoadTestImage
			// 
			this->buttonLoadTestImage->Location = System::Drawing::Point(440, 10);
			this->buttonLoadTestImage->Name = L"buttonLoadTestImage";
			this->buttonLoadTestImage->Size = System::Drawing::Size(25, 23);
			this->buttonLoadTestImage->TabIndex = 3;
			this->buttonLoadTestImage->Text = L"...";
			this->buttonLoadTestImage->UseVisualStyleBackColor = true;
			this->buttonLoadTestImage->Click += gcnew System::EventHandler(this, &Form1::buttonLoadTestImage_Click);
			// 
			// labelFilePath
			// 
			this->labelFilePath->AutoSize = true;
			this->labelFilePath->Location = System::Drawing::Point(8, 12);
			this->labelFilePath->Name = L"labelFilePath";
			this->labelFilePath->Size = System::Drawing::Size(67, 17);
			this->labelFilePath->TabIndex = 2;
			this->labelFilePath->Text = L"File Path:";
			// 
			// textBoxImagePath
			// 
			this->textBoxImagePath->Location = System::Drawing::Point(76, 10);
			this->textBoxImagePath->Name = L"textBoxImagePath";
			this->textBoxImagePath->Size = System::Drawing::Size(358, 22);
			this->textBoxImagePath->TabIndex = 1;
			this->textBoxImagePath->Text = L"data\\am.jpg";
			// 
			// groupBoxFirooz
			// 
			this->groupBoxFirooz->Controls->Add(this->buttonFiroozTest);
			this->groupBoxFirooz->Controls->Add(this->groupBoxFiroozDarkness);
			this->groupBoxFirooz->Controls->Add(this->groupBoxFiroozHat);
			this->groupBoxFirooz->Location = System::Drawing::Point(6, 35);
			this->groupBoxFirooz->Name = L"groupBoxFirooz";
			this->groupBoxFirooz->Size = System::Drawing::Size(459, 104);
			this->groupBoxFirooz->TabIndex = 0;
			this->groupBoxFirooz->TabStop = false;
			this->groupBoxFirooz->Text = L"Haji Firooz";
			// 
			// buttonFiroozTest
			// 
			this->buttonFiroozTest->Location = System::Drawing::Point(6, 50);
			this->buttonFiroozTest->Name = L"buttonFiroozTest";
			this->buttonFiroozTest->Size = System::Drawing::Size(91, 30);
			this->buttonFiroozTest->TabIndex = 1;
			this->buttonFiroozTest->Text = L"Make Firooz";
			this->buttonFiroozTest->UseVisualStyleBackColor = true;
			this->buttonFiroozTest->Click += gcnew System::EventHandler(this, &Form1::buttonFiroozTest_Click);
			// 
			// groupBoxFiroozDarkness
			// 
			this->groupBoxFiroozDarkness->Controls->Add(this->radioButtonFiroozDarkLow);
			this->groupBoxFiroozDarkness->Controls->Add(this->radioButtonFiroozDarkMedium);
			this->groupBoxFiroozDarkness->Controls->Add(this->radioButtonFiroozDarkHigh);
			this->groupBoxFiroozDarkness->Location = System::Drawing::Point(358, 10);
			this->groupBoxFiroozDarkness->Name = L"groupBoxFiroozDarkness";
			this->groupBoxFiroozDarkness->Size = System::Drawing::Size(91, 88);
			this->groupBoxFiroozDarkness->TabIndex = 0;
			this->groupBoxFiroozDarkness->TabStop = false;
			this->groupBoxFiroozDarkness->Text = L"Darkness";
			// 
			// radioButtonFiroozDarkLow
			// 
			this->radioButtonFiroozDarkLow->AutoSize = true;
			this->radioButtonFiroozDarkLow->Location = System::Drawing::Point(6, 61);
			this->radioButtonFiroozDarkLow->Name = L"radioButtonFiroozDarkLow";
			this->radioButtonFiroozDarkLow->Size = System::Drawing::Size(60, 21);
			this->radioButtonFiroozDarkLow->TabIndex = 0;
			this->radioButtonFiroozDarkLow->TabStop = true;
			this->radioButtonFiroozDarkLow->Text = L"Light";
			this->radioButtonFiroozDarkLow->UseVisualStyleBackColor = true;
			// 
			// radioButtonFiroozDarkMedium
			// 
			this->radioButtonFiroozDarkMedium->AutoSize = true;
			this->radioButtonFiroozDarkMedium->Location = System::Drawing::Point(6, 40);
			this->radioButtonFiroozDarkMedium->Name = L"radioButtonFiroozDarkMedium";
			this->radioButtonFiroozDarkMedium->Size = System::Drawing::Size(78, 21);
			this->radioButtonFiroozDarkMedium->TabIndex = 0;
			this->radioButtonFiroozDarkMedium->TabStop = true;
			this->radioButtonFiroozDarkMedium->Text = L"Medium";
			this->radioButtonFiroozDarkMedium->UseVisualStyleBackColor = true;
			// 
			// radioButtonFiroozDarkHigh
			// 
			this->radioButtonFiroozDarkHigh->AutoSize = true;
			this->radioButtonFiroozDarkHigh->Checked = true;
			this->radioButtonFiroozDarkHigh->Location = System::Drawing::Point(6, 20);
			this->radioButtonFiroozDarkHigh->Name = L"radioButtonFiroozDarkHigh";
			this->radioButtonFiroozDarkHigh->Size = System::Drawing::Size(59, 21);
			this->radioButtonFiroozDarkHigh->TabIndex = 0;
			this->radioButtonFiroozDarkHigh->TabStop = true;
			this->radioButtonFiroozDarkHigh->Text = L"Dark";
			this->radioButtonFiroozDarkHigh->UseVisualStyleBackColor = true;
			// 
			// groupBoxFiroozHat
			// 
			this->groupBoxFiroozHat->Controls->Add(this->radioButtonHat3);
			this->groupBoxFiroozHat->Controls->Add(this->radioButtonHat2);
			this->groupBoxFiroozHat->Controls->Add(this->radioButtonHat1);
			this->groupBoxFiroozHat->Location = System::Drawing::Point(163, 10);
			this->groupBoxFiroozHat->Name = L"groupBoxFiroozHat";
			this->groupBoxFiroozHat->Size = System::Drawing::Size(129, 88);
			this->groupBoxFiroozHat->TabIndex = 0;
			this->groupBoxFiroozHat->TabStop = false;
			this->groupBoxFiroozHat->Text = L"Firooz Hat";
			// 
			// radioButtonHat3
			// 
			this->radioButtonHat3->AutoSize = true;
			this->radioButtonHat3->Location = System::Drawing::Point(6, 61);
			this->radioButtonHat3->Name = L"radioButtonHat3";
			this->radioButtonHat3->Size = System::Drawing::Size(88, 21);
			this->radioButtonHat3->TabIndex = 0;
			this->radioButtonHat3->TabStop = true;
			this->radioButtonHat3->Text = L"Rain Bow";
			this->radioButtonHat3->UseVisualStyleBackColor = true;
			// 
			// radioButtonHat2
			// 
			this->radioButtonHat2->AutoSize = true;
			this->radioButtonHat2->Location = System::Drawing::Point(6, 40);
			this->radioButtonHat2->Name = L"radioButtonHat2";
			this->radioButtonHat2->Size = System::Drawing::Size(119, 21);
			this->radioButtonHat2->TabIndex = 0;
			this->radioButtonHat2->TabStop = true;
			this->radioButtonHat2->Text = L"Red Papanoel";
			this->radioButtonHat2->UseVisualStyleBackColor = true;
			// 
			// radioButtonHat1
			// 
			this->radioButtonHat1->AutoSize = true;
			this->radioButtonHat1->Checked = true;
			this->radioButtonHat1->Location = System::Drawing::Point(6, 20);
			this->radioButtonHat1->Name = L"radioButtonHat1";
			this->radioButtonHat1->Size = System::Drawing::Size(98, 21);
			this->radioButtonHat1->TabIndex = 0;
			this->radioButtonHat1->TabStop = true;
			this->radioButtonHat1->Text = L"Red Firooz";
			this->radioButtonHat1->UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(920, 539);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->buttonTest);
			this->Name = L"Form1";
			this->Text = L"Photo Analyzer";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->tabControl1->ResumeLayout(false);
			this->tabPageTest->ResumeLayout(false);
			this->tabPageTest->PerformLayout();
			this->groupBoxCanny->ResumeLayout(false);
			this->groupBoxPencil->ResumeLayout(false);
			this->groupBoxCartoon->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxTest))->EndInit();
			this->groupBoxTextAdder->ResumeLayout(false);
			this->groupBoxTextAdder->PerformLayout();
			this->groupBoxFirooz->ResumeLayout(false);
			this->groupBoxFiroozDarkness->ResumeLayout(false);
			this->groupBoxFiroozDarkness->PerformLayout();
			this->groupBoxFiroozHat->ResumeLayout(false);
			this->groupBoxFiroozHat->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
				 hajiMaker = new hajiFirooz(3);
			 }
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		/*   //Mat to Bitmap Test
		std::string imagePath = msclr::interop::marshal_as<std::string>(textBoxImagePath->Text);
		MatBitmapWrapper ^ mywrapper = gcnew MatBitmapWrapper();

		cv::Mat imsrc = imread(imagePath);
		Bitmap ^ bmp = mywrapper->Mat2Bmp(imsrc);

		pictureBoxTest->Image = bmp;
		*/


		//Bitmap 2 Mat Test
		/*
		MatBitmapWrapper ^ mywrapper = gcnew MatBitmapWrapper();
		Bitmap ^bmp = ( Bitmap ^)Bitmap::FromFile(textBoxImagePath->Text);
		Mat im = mywrapper->Bmp2Mat(bmp);
		imshow("im", im);
		waitKey(1);
		*/
		//webapi interface test
		/*
		Bitmap ^bmp = (Bitmap ^)Bitmap::FromFile(textBoxImagePath->Text);
		webAPIInterface ^ webint = gcnew webAPIInterface();
		Bitmap ^ cartoon = webint->cartoofify(bmp, 1);
		pictureBoxTest->Image = cartoon;
		*/
			 }
	private: System::Void buttonLoadTestImage_Click(System::Object^  sender, System::EventArgs^  e) {

				 if (OFDTestImage->ShowDialog()== System::Windows::Forms::DialogResult::OK)
				 {					 
					 textBoxImagePath->Text = OFDTestImage->FileName;
				 }
			 }
private: System::Void buttonFiroozTest_Click(System::Object^  sender, System::EventArgs^  e) {
			 std::string imagePath = msclr::interop::marshal_as<std::string>(textBoxImagePath->Text);
			 
			 cv::Mat imsrc = imread(imagePath);
			 cv::Mat imout = imsrc.clone();
			 int runtime = 0;
			 int hatTypeNumber=-1;
			 int darkness =1;
			 if (radioButtonHat1->Checked == true)
			 {
				 hatTypeNumber = 1;
			 }
			 else if (radioButtonHat2->Checked == true)
			 {
				 hatTypeNumber = 2;
			 }
			 else if (radioButtonHat3->Checked == true)
			 {
				 hatTypeNumber = 3;
			 }

			 if (radioButtonFiroozDarkHigh->Checked == true)
			 {
				 darkness = 1;
			 }
			 else if (radioButtonFiroozDarkMedium->Checked == true)
			 {
				 darkness = 2;
			 }
			 else if (radioButtonFiroozDarkLow->Checked == true)
			 {
				 darkness = 3;
			 }
			 hajiMaker->makeFirooz(imsrc,imout,1,hatTypeNumber,darkness);
			 imshow("firooz",imout);
			 waitKey(1);
		 
		 }
private: System::Void textBoxTextAdderText_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void labelTextAdder_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void buttonAdderText_Click(System::Object^  sender, System::EventArgs^  e) {

			Bitmap ^bmp = gcnew Bitmap(textBoxImagePath->Text);
			textAdder ^adder = gcnew textAdder();
			adder->addText(bmp,textBoxAdderText->Text, PointF(Convert::ToSingle(textBoxAdderX->Text),Convert::ToSingle(textBoxAdderY->Text)),Convert::ToUInt32(textBoxAdderFontSize->Text) );
			pictureBoxTest->Image = bmp;
			bmp->Save(textBoxAdderOut->Text);
		 }
private: System::Void buttonCartoonMaker_Click(System::Object^  sender, System::EventArgs^  e) {
	std::string imagePath = msclr::interop::marshal_as<std::string>(textBoxImagePath->Text);

	cv::Mat imsrc = imread(imagePath);
	cv::Mat imout = imsrc.clone();
	
	//detailEnhance(imsrc, imsrc);
	//edgePreservingFilter(imsrc, imsrc);
	//blur(imsrc, imsrc,cv::Size(15, 15));
	cv::stylization(imsrc,imout,60,0.9);
	cv::imshow("Cartoon", imout);
	cv::imwrite("data/Cout.jpg",imout);
	
	cv::waitKey(1);

}
private: System::Void buttonPencil_Click(System::Object^  sender, System::EventArgs^  e) {
	std::string imagePath = msclr::interop::marshal_as<std::string>(textBoxImagePath->Text);

	cv::Mat imsrc = imread(imagePath);
	cv::Mat imout,o2 = imsrc.clone();

	//detailEnhance(imsrc, imsrc);
	//edgePreservingFilter(imsrc, imsrc);
	//blur(imsrc, imsrc,cv::Size(15, 15));
	//blur(src_gray, detected_edges, Size(3, 3))
	cv::pencilSketch(imsrc, imout, o2,60, 0.18,0.03);
	cv::imshow("p1", imout);
	cv::imshow("p2", o2);
	cv::imwrite("data/pencil.jpg", imout);
	cv::imwrite("data/pencil2.jpg", o2);
	cv::waitKey(1);
}
private: System::Void buttonCanny_Click(System::Object^  sender, System::EventArgs^  e) {

	std::string imagePath = msclr::interop::marshal_as<std::string>(textBoxImagePath->Text);

	cv::Mat imsrc = imread(imagePath);
	
	cv::Mat grey = Mat(imsrc.rows,imsrc.cols,CV_8U);
	blur(grey, grey, cv::Size(21, 21));

	cvtColor(imsrc, grey, CV_RGB2GRAY);
	Mat imout = grey.clone();
	cv::Mat neg = grey.clone();
	neg = 255 - grey;
	blur(neg, neg, cv::Size(7,7 ));
	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y;



	//Sobel 
	int scale = 1;
	int delta = 0;
	/// Gradient X
	//Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
	Sobel(grey, grad_x, CV_16S, 1, 0, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_x, abs_grad_x);

	/// Gradient Y
	//Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
	Sobel(grey, grad_y, CV_16S, 0, 1, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_y, abs_grad_y);
	addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, imout);
	imout = 255 - imout;

	for (size_t i = 0; i < grey.rows; i++)
	{
		for (size_t j = 0; j < grey.cols; j++)
		{
			
			int idx = i*grey.step + j*grey.channels();
			imout.data[idx] = ChannelBlend_Phoenix(imout.data[idx], 255-grey.data[idx], 0.8);
			//((neg.data[idx] == 255) ? neg.data[idx] : min(255, ((grey.data[idx] * 255) / (255 - neg.data[idx]))  )  );
			//((grey.data[idx] == 255) ? grey.data[idx] : min(255, ((neg.data[idx] << 8) / (255 - grey.data[idx]))));

		}

	}

	//((B[idx] == 255) ? B[idx] : min(255, ((A[idx] << 8) / (255 - B[idx]))))
	//addWeighted(grey, 0.5, neg, 0.5, 0.0, imout);
	

	cv::imshow("Canny", imout); 

	cv::imwrite("data/pencil.jpg", imout);
	
	cv::waitKey(1);
}
private: System::Void button1_Click_1(System::Object^  sender, System::EventArgs^  e) {

	std::string imagePath = msclr::interop::marshal_as<std::string>(textBoxImagePath->Text);

	cv::Mat imsrc = imread(imagePath);
	Mat imout = imsrc.clone();
	oilEffect *ol = new oilEffect();
	ol->Process(&imsrc, 2, 255, &imout);

	/*
	cv::Mat grey = Mat(imsrc.rows,imsrc.cols,CV_8U);

	cvtColor(imsrc, grey, CV_RGB2GRAY);
	Mat imout = grey.clone();
	cv::Mat neg = grey.clone();
	neg = 255 - grey;
	blur(neg, neg, cv::Size(7,7 ));

	for (size_t i = 0; i < grey.rows; i++)
	{
	for (size_t j = 0; j < grey.cols; j++)
	{

	int idx = i*grey.step + j*grey.channels();
	imout.data[idx] = ChannelBlend_Multiply(neg.data[idx], grey.data[idx]);
	//((neg.data[idx] == 255) ? neg.data[idx] : min(255, ((grey.data[idx] * 255) / (255 - neg.data[idx]))  )  );
	//((grey.data[idx] == 255) ? grey.data[idx] : min(255, ((neg.data[idx] << 8) / (255 - grey.data[idx]))));

	}

	}

	//((B[idx] == 255) ? B[idx] : min(255, ((A[idx] << 8) / (255 - B[idx]))))
	//addWeighted(grey, 0.5, neg, 0.5, 0.0, imout);
	*/

	cv::imshow("oil", imout);

	cv::imwrite("data/oil.jpg", imout);

	cv::waitKey(1);
}
private: System::Void buttonFlagger_Click(System::Object^  sender, System::EventArgs^  e) {
	flagger *fg = new flagger(2);
	std::string imagePath = msclr::interop::marshal_as<std::string>(textBoxImagePath->Text);

	cv::Mat imsrc = imread(imagePath);
	Mat imout = imsrc.clone();
	fg->test(imsrc, &imout, 1);
	imwrite("data/flagOut.jpg", imout);
	
}
};
}

