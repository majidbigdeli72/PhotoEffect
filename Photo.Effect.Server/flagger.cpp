#include "StdAfx.h"
#include "flagger.h"
#include "photoShopBlends.h"
static const char* const path = "data/0.jpg";

void flagger::loadFlags(string XMLFilePath, flagSpec *flagData){
	FileStorage fs(XMLFilePath, FileStorage::READ);

	flagData->filePath = fs["filePath"];
	flagData->Ratio = fs["hatRatio"];
	flagData->xoffset = fs["xoffset"];
	flagData->yoffset = fs["yoffset"];

	fs.release();
	return;
}

flagger::flagger(int flagNumbers)
{
	char xmlfilePath[100];
	for (int i = 0; i<flagNumbers; i++)
	{
		sprintf_s(xmlfilePath, "data/f%d.xml", i + 1);
		flagSpec *fs = new flagSpec();
		loadFlags(xmlfilePath, fs);
		fs->flagImg = imread(fs->filePath, CV_LOAD_IMAGE_UNCHANGED | CV_LOAD_IMAGE_ANYDEPTH);
		flags.push_back(fs);
	}


}


flagger::~flagger(void)
{
}


void flagger::addFlag(Mat *img, cv::Rect flagBox, int hatNumber){

	double headWidth = (double)flagBox.width*flags[hatNumber]->Ratio;
	flagI = flags[hatNumber]->flagImg.clone();
	double ratio = (double)headWidth / (double)flagI.cols;
	resize(flagI, flagI, cv::Size(), ratio, ratio);
	int hstart = flagBox.y + flagI.rows*flags[hatNumber]->yoffset;
	int wstart = flagBox.x - (flagI.cols - flagBox.width) / 2 + flagI.cols*flags[hatNumber]->xoffset;
	int hstartDiff = 0;
	if (hstart <0)
	{
		hstartDiff = 0 - hstart;
	}

	for (int i = hstart + hstartDiff, ii = hstartDiff; ii<flagI.rows; i++, ii++)
	{
		for (int j = wstart, jj = 0; jj<flagI.cols; j++, jj++)
		{
			if (flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 3] >50)
			{
				img->data[(i*img->cols + j) * 3] = flagI.data[(ii*flagI.cols + jj)*flagI.channels()];
				img->data[(i*img->cols + j) * 3 + 1] = flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 1];
				img->data[(i*img->cols + j) * 3 + 2] = flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 2];
			}

		}
	}

}


void flagger::addFlag2(Mat *img, cv::Rect flagBox, int hatNumber){

	double headWidth = (double)flagBox.width*flags[hatNumber]->Ratio;
	flagI = flags[hatNumber]->flagImg.clone();
	double ratio = (double)headWidth / (double)flagI.cols;
	resize(flagI, flagI, cv::Size(), ratio, ratio);
	int hstart = flagBox.y + flagI.rows*flags[hatNumber]->yoffset;
	int wstart = flagBox.x - (flagI.cols - flagBox.width) / 2 + flagI.cols*flags[hatNumber]->xoffset;
	int hstartDiff = 0;
	if (hstart <0)
	{
		hstartDiff = 0 - hstart;
	}

	for (int i = hstart + hstartDiff, ii = hstartDiff; ii<flagI.rows; i++, ii++)
	{
		for (int j = wstart, jj = 0; jj<flagI.cols; j++, jj++)
		{
			if (flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 3] >50)
			{
				
				img->data[(i*img->cols + j) * 3] = ChannelBlend_Overlay(flagI.data[(ii*flagI.cols + jj)*flagI.channels()], img->data[(i*img->cols + j) * 3]);
				img->data[(i*img->cols + j) * 3 + 1] = ChannelBlend_Overlay(flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 1], img->data[(i*img->cols + j) * 3 + 1]);
				img->data[(i*img->cols + j) * 3 + 2] = ChannelBlend_Overlay(flagI.data[(ii*flagI.cols + jj)*flagI.channels() + 2], img->data[(i*img->cols + j) * 3 + 2]);
			}

		}
	}

}


void flagger::addFlag3(Mat *img, cv::Rect flagBox, int flagNumber){

	double headWidth = (double)flagBox.width*flags[flagNumber]->Ratio;
	flagI = flags[flagNumber]->flagImg.clone();
	Mat flag2 = imread("data/p2_2.jpg");



	double ratio = (double)headWidth / (double)flagI.cols;
	resize(flagI, flagI, cv::Size(), ratio, ratio);
	cvtColor(flagI, flagDist, CV_RGBA2GRAY);
	resize(flag2, flag2, cv::Size(), ratio, ratio);

	for (int i = 0; i<flagI.rows; i++)
	{
		for (int j = 0; j<flagI.cols; j++)
		{
			flagDist.data[i*flagDist.step + j*flagDist.channels()] = flagI.data[i*flagI.step + j*flagI.channels() + 3];
		}
	}
	//cvtColor(flagI, flagI, CV_RGBA2RGB);

	//Mat src_mask = 255 * Mat::ones(hatI.rows, hatI.cols, hatI.depth());
	threshold(flagDist, flagDist, 1, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

	copyMakeBorder(flagDist, flagDist, 5, 5, 5, 5, BORDER_CONSTANT, 0);
	copyMakeBorder(flag2, flag2, 5, 5, 5, 5, BORDER_CONSTANT, 0);
	//copyMakeBorder(maskEyes, maskEyes, 2, 2, 2, 2, BORDER_CONSTANT, 0);

	int erosion_size = 3;
	//dilation
	Mat element = getStructuringElement(MORPH_ELLIPSE,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));
	dilate(flagDist, flagDist, element);
#ifdef _DEBUG
	imshow("hatMask", flagDist);
	imshow("flag", flagI);
	imwrite("data/flag.jpg", flagI);

	waitKey(1);
#endif
	int x = flagBox.x + flagBox.width / 2;// +flagBox.width*flags[flagNumber]->xoffset;
	int y = flagBox.y + flagBox.height / 2;// +flagBox.height*flags[flagNumber]->yoffset;
	cv::Point center(x, y);

	seamlessClone(flag2, *img, flagDist, center, *img, NORMAL_CLONE);
	//circle(*img, center, 10, Scalar(255, 255, 255), 5);
}

bool flagger::test(Mat imgscr,Mat *imgdes,int faceNumbers){
	
	try{
	
		cvtColor(imgscr,img,CV_RGB2GRAY);
		//imgdes = imgscr.clone();

		if (!img.data)
		{
			throw("Cannot load the image \n");
			//exit(1);
		}

		int foundface;
		float landmarks[2 * stasm_NLANDMARKS]; // x,y coords (note the 2)

		if (!stasm_search_single(&foundface, landmarks,
			(const char*)img.data, img.cols, img.rows, path, "data"))
		{
			throw ("Error in stasm: %s \n",stasm_lasterr());
			//exit(1);
		}
		

		if (!foundface)
			throw("No face found in %s\n", path);
		else
		{
			
			faceRect=makeFaceBox(landmarks);
			cv::Point pFace(faceRect.x,faceRect.y);			
			effectMask = Mat(faceRect.height,faceRect.width,CV_8U,0.0);
			int index1[4] = { 0, 37, 50, 59 };
			int ilen = 4;
#ifdef _DEBUG
			for (int i = 0; i < ilen; i++){
				line(img, cv::Point(cvRound(landmarks[index1[i] * 2]), cvRound(landmarks[index1[i] * 2 + 1])),
					cv::Point(cvRound(landmarks[(index1[i] + 1) * 2]), cvRound(landmarks[(index1[i] + 1) * 2 + 1])),
					cv::Scalar(255,255,255),2);
			}

#endif
			ROI_Poly.clear();
			ROI_Vertices.clear();
			for (int i = 0; i < ilen; i++){
				ROI_Vertices.push_back(cv::Point(cvRound(landmarks[index1[i] * 2]), cvRound(landmarks[index1[i] * 2 + 1])) - pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8,0);
			Rect flagRect= boundingRect(effectMask);
			flagRect.x += faceRect.x;
			flagRect.y += faceRect.y;
			rectangle(img, flagRect, Scalar(255, 0, 0), 2);
			addFlag2(imgdes, flagRect, 0);

			//Mat ffo =(*imgdes)(flagRect);
			//blur(ffo, ffo, Size(5, 5));
			//Mat flag = imread("data\pf.png");


			/*
#ifdef _DEBUG
			for (int i = 16; i < 21; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif

			ROI_Poly.clear();
			ROI_Vertices.clear();
			for (int i = 16; i < 22; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);                 

#ifdef _DEBUG
			for (int i = 22; i <27 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif

			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 22; i < 28; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);

			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);   

#ifdef _DEBUG
			for (int i = 30; i <38 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif


			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 30; i < 38; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  

#ifdef _DEBUG
			for (int i = 39; i <47 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif

			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 40; i < 47; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  

#ifdef _DEBUG
			for (int i = 59; i <76 ; i++){
				line(img, cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1])),
					cv::Point( cvRound(landmarks[(i+1)*2]),cvRound(landmarks[(i+1)*2+1])),
					cv::Scalar(255,255,255),2);
			}
#endif
			ROI_Vertices.clear();
			ROI_Poly.clear();
			for (int i = 72; i < 77; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			for (int i = 59; i < 66; i++){
				ROI_Vertices.push_back( cv::Point( cvRound(landmarks[i*2]),cvRound(landmarks[i*2+1]))-pFace);
			}
			approxPolyDP(ROI_Vertices, ROI_Poly, 1.0, true);
			fillConvexPoly(effectMask, &ROI_Poly[0], ROI_Poly.size(), 255, 8, 0);  

			*/
				
			
#ifdef _DEBUG
			imshow("MASKDEBUG",effectMask);
			imshow("IMG ASM", img);
#endif
		}

		//*runtime = *runtime - timeGetTime();
#ifdef _DEBUG
		cv::imshow("desImageDEUG", *imgdes);
		cv::imshow("sourceIMGDebug", imgscr);	
		cv::imshow("ASMDEBUG", img);	
		cv::waitKey(1);
#endif
		return 1;
	}
	catch(std::exception e) {
		e.what();
		return 0;
	}
	

}