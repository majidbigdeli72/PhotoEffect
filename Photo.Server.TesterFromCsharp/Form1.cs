﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PhotoServerLib;
using Photo.Effect.DataContract;
using Parmik.CS.Core.Image;
using System.Net.Http;
namespace Photo.Server.TesterFromCsharp
{
    public partial class mainForm : Form
    {
        static webAPIInterface intTest = new webAPIInterface("data");
        public mainForm()
        {
            InitializeComponent();
        }

        private void buttonSetImagePath_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBoxImagePAth.Text = ofd.FileName;
                
            }
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            Bitmap bmp =(Bitmap) Bitmap.FromFile(textBoxImagePAth.Text);

          
            Bitmap result = intTest.cartoofify(bmp,.5f);
            pictureBox1.Image = result;
            
        }

        private void buttonAPI_Click(object sender, EventArgs e)
        {
            Bitmap bmp =(Bitmap) Bitmap.FromFile(textBoxImagePAth.Text);
            using (var client = new HttpClient())
                    {
                        var model = new SimplePhotoEffect()
                        {

                            Effect = Effects.hajiFiroozMaker,
                            ImagesBase64 = BitmapConvert.ToBase64(bmp),
                            Level = 0,
                            Text = "2",
                            //Signature = "یتاشسی"
                            
                        };


                        //var content = new FormUrlEncodedContent();
                        using (
                            var response =
                                
                                    client.PostAsJsonAsync("http://core.effect.bot.parmik.com/api/effect/Effect", model).Result)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                var result =  response.Content.ReadAsAsync<PhotoResponse>().Result;
                                var image = BitmapConvert.GetImage(result.ImageBase64);
                                             pictureBox1.Image = image;
                            }
                        }}
        }

        private void buttonFlagger_Click(object sender, EventArgs e)
        {
            Bitmap bmp = (Bitmap)Bitmap.FromFile(textBoxImagePAth.Text);

            //webAPIInterface intTest = new webAPIInterface();
            Bitmap result = intTest.flagIt(bmp, 1f);
            pictureBox1.Image = result;
        }

        private void buttonTextAdder_Click(object sender, EventArgs e)
        {
            //Bitmap bmp = (Bitmap)Bitmap.FromFile(textBoxImagePAth.Text);
            //webAPIInterface intTest = new webAPIInterface();
           // Bitmap result = intTest.addText("ستالشسلنتبدو سنیا سیت سشیت سش عهشساین هنشع ", "   سیبعهسید هعتسب  اهسیب   هعسیب  هعسذیب  سلام ای کهنه عشق من که یاد تو چه پا بر جاست سلام ای نازنین من", 1);
            Bitmap result = intTest.addText("ستالشسلنتبدو سنیا سیت سشیت سش عهشساین هنشع ", "2", 1);
          
            pictureBox1.Image = result;

        }

        private void buttonSaveImage_Click(object sender, EventArgs e)
        {
            pictureBox1.Image.Save("out.jpg");
        }

        private void buttonHajiMaker_Click(object sender, EventArgs e)
        {
            Bitmap bmp = (Bitmap)Bitmap.FromFile(textBoxImagePAth.Text);
            Bitmap result = intTest.hajiMaker(bmp,2,0);
            pictureBox1.Image = result;

        }
    }
}
