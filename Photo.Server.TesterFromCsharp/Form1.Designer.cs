﻿namespace Photo.Server.TesterFromCsharp
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSetImagePath = new System.Windows.Forms.Button();
            this.textBoxImagePAth = new System.Windows.Forms.TextBox();
            this.buttonTest = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.buttonAPI = new System.Windows.Forms.Button();
            this.buttonFlagger = new System.Windows.Forms.Button();
            this.buttonTextAdder = new System.Windows.Forms.Button();
            this.buttonSaveImage = new System.Windows.Forms.Button();
            this.buttonHajiMaker = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSetImagePath
            // 
            this.buttonSetImagePath.Location = new System.Drawing.Point(269, 12);
            this.buttonSetImagePath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetImagePath.Name = "buttonSetImagePath";
            this.buttonSetImagePath.Size = new System.Drawing.Size(40, 23);
            this.buttonSetImagePath.TabIndex = 0;
            this.buttonSetImagePath.Text = "...";
            this.buttonSetImagePath.UseVisualStyleBackColor = true;
            this.buttonSetImagePath.Click += new System.EventHandler(this.buttonSetImagePath_Click);
            // 
            // textBoxImagePAth
            // 
            this.textBoxImagePAth.Location = new System.Drawing.Point(24, 12);
            this.textBoxImagePAth.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxImagePAth.Name = "textBoxImagePAth";
            this.textBoxImagePAth.Size = new System.Drawing.Size(239, 22);
            this.textBoxImagePAth.TabIndex = 1;
            // 
            // buttonTest
            // 
            this.buttonTest.Location = new System.Drawing.Point(24, 41);
            this.buttonTest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(111, 33);
            this.buttonTest.TabIndex = 2;
            this.buttonTest.Text = "Cartoonify";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(24, 102);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(788, 667);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // buttonAPI
            // 
            this.buttonAPI.Location = new System.Drawing.Point(569, 42);
            this.buttonAPI.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAPI.Name = "buttonAPI";
            this.buttonAPI.Size = new System.Drawing.Size(75, 32);
            this.buttonAPI.TabIndex = 4;
            this.buttonAPI.Text = "buttonapi";
            this.buttonAPI.UseVisualStyleBackColor = true;
            this.buttonAPI.Click += new System.EventHandler(this.buttonAPI_Click);
            // 
            // buttonFlagger
            // 
            this.buttonFlagger.Location = new System.Drawing.Point(142, 42);
            this.buttonFlagger.Margin = new System.Windows.Forms.Padding(4);
            this.buttonFlagger.Name = "buttonFlagger";
            this.buttonFlagger.Size = new System.Drawing.Size(100, 33);
            this.buttonFlagger.TabIndex = 5;
            this.buttonFlagger.Text = "Flag It";
            this.buttonFlagger.UseVisualStyleBackColor = true;
            this.buttonFlagger.Click += new System.EventHandler(this.buttonFlagger_Click);
            // 
            // buttonTextAdder
            // 
            this.buttonTextAdder.Location = new System.Drawing.Point(249, 42);
            this.buttonTextAdder.Name = "buttonTextAdder";
            this.buttonTextAdder.Size = new System.Drawing.Size(117, 33);
            this.buttonTextAdder.TabIndex = 6;
            this.buttonTextAdder.Text = "text Adder";
            this.buttonTextAdder.UseVisualStyleBackColor = true;
            this.buttonTextAdder.Click += new System.EventHandler(this.buttonTextAdder_Click);
            // 
            // buttonSaveImage
            // 
            this.buttonSaveImage.Location = new System.Drawing.Point(326, 11);
            this.buttonSaveImage.Name = "buttonSaveImage";
            this.buttonSaveImage.Size = new System.Drawing.Size(125, 25);
            this.buttonSaveImage.TabIndex = 7;
            this.buttonSaveImage.Text = "Save Image";
            this.buttonSaveImage.UseVisualStyleBackColor = true;
            this.buttonSaveImage.Click += new System.EventHandler(this.buttonSaveImage_Click);
            // 
            // buttonHajiMaker
            // 
            this.buttonHajiMaker.Location = new System.Drawing.Point(372, 41);
            this.buttonHajiMaker.Name = "buttonHajiMaker";
            this.buttonHajiMaker.Size = new System.Drawing.Size(105, 34);
            this.buttonHajiMaker.TabIndex = 8;
            this.buttonHajiMaker.Text = "Haji Maker";
            this.buttonHajiMaker.UseVisualStyleBackColor = true;
            this.buttonHajiMaker.Click += new System.EventHandler(this.buttonHajiMaker_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 793);
            this.Controls.Add(this.buttonHajiMaker);
            this.Controls.Add(this.buttonSaveImage);
            this.Controls.Add(this.buttonTextAdder);
            this.Controls.Add(this.buttonFlagger);
            this.Controls.Add(this.buttonAPI);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.textBoxImagePAth);
            this.Controls.Add(this.buttonSetImagePath);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "mainForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSetImagePath;
        private System.Windows.Forms.TextBox textBoxImagePAth;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Windows.Forms.Button buttonAPI;
        private System.Windows.Forms.Button buttonFlagger;
        private System.Windows.Forms.Button buttonTextAdder;
        private System.Windows.Forms.Button buttonSaveImage;
        private System.Windows.Forms.Button buttonHajiMaker;
    }
}

