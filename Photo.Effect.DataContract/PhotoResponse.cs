﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Parmik.CS.Core.Image;

namespace Photo.Effect.DataContract
{
    [DataContract]
    public class PhotoResponse
    {
        [DataMember]
        public ResponseTypes Type { get; set; }
        [DataMember]
        public string ImageBase64 { get; set; }
        public string ErrorMessage { get; set; }
   

        public Image Image()
        {
            return BitmapConvert.GetImage(ImageBase64);
        }
    }
}
