﻿using System.Runtime.Serialization;

namespace Photo.Effect.DataContract
{
 [DataContract]   
    public enum ResponseTypes:int
    {
        Ok=0,
        Fail=1,
        FailUnknownEffect=2, 
        FailCSException=3
    }
}