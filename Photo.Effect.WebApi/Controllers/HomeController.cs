﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Photo.Effect.WebApi.Controllers
{
    public class HomeController : ApiController
    {
        // GET: Home
        public string Get()
        {
            return HttpContext.Current.Server.MapPath("../../data/");
        }
        public string Get2()
        {
            return     HttpContext.Current.Server.MapPath(@"../../data/reqs/" + DateTime.Now.ToString("dd-HH-mm-ss-fff")+".json");
    ;
        }
    }
}