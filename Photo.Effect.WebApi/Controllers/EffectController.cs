﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Parmik.CS.Core.Image;
using Photo.Effect.DataContract;
using System.Web;
using PhotoServerLib;
using Newtonsoft.Json;


namespace Photo.Effect.WebApi.Controllers
{
    public class EffectController : ApiController
    {

         PhotoServerLib.webAPIInterface effectInterface = new webAPIInterface(HttpContext.Current.Server.MapPath("../../data"));
        //static PhotoServerLib.webAPIInterface effectInterface = new webAPIInterface(Path.Combine(HttpRuntime.AppDomainAppPath, "data"));


        [HttpPost]
        public PhotoResponse Test(SimplePhotoEffect photo)
        {
            return new PhotoResponse() { Type = ResponseTypes.Ok, ImageBase64 = photo.ImagesBase64 };
        }
        [HttpPost]
        public PhotoResponse Effect(SimplePhotoEffect photo)
        {

            try
            {
                String FilePath = HttpContext.Current.Server.MapPath(@"../../data/reqs/" + DateTime.Now.ToString("dd-HH-mm-ss-fff") + ".json");
                //DateTime.Now.ToString("dd-HH-mm-ss");
                System.String data = JsonConvert.SerializeObject(photo);
                File.WriteAllText(FilePath, data);
                /*
                if (photo.Effect == Effects.Cartoonify)
                {
                    var srcimg = BitmapConvert.GetImage(photo.ImagesBase64);
                    var target = effectInterface.cartoofify(srcimg, photo.Level ?? .5f);
                    return new PhotoResponse() { Type = ResponseTypes.Ok, ImageBase64 = BitmapConvert.ToBase64(target) };

                }


                else if (photo.Effect == Effects.Flagging)
                {
                    // string filepath = HttpContext.Current.Server.MapPath("~/data"); 
                    var srcimg = BitmapConvert.GetImage(photo.ImagesBase64);
                    var target = effectInterface.flagIt(srcimg, photo.Level ?? 1.0f);
                    return new PhotoResponse() { Type = ResponseTypes.Ok, ImageBase64 = BitmapConvert.ToBase64(target) };

                }
                else */
                if (photo.Effect == Effects.TextAdder)
                {
                    // string filepath = HttpContext.Current.Server.MapPath("~/data"); 
                    //var srcimg = BitmapConvert.GetImage(photo.ImagesBase64);
                    //if (photo.Text =="1")
                    //{
                    var target = effectInterface.addText(photo.Signature ?? "ارادتمند", photo.Text ?? "1", (int)(photo.Level ?? 1.0f));
                    return new PhotoResponse() { Type = ResponseTypes.Ok, ImageBase64 = BitmapConvert.ToBase64(target) };
                    //}



                }
                else if (photo.Effect == Effects.hajiFiroozMaker)
                {
                    // string filepath = HttpContext.Current.Server.MapPath("~/data"); 

                    var srcimg = BitmapConvert.GetImage(photo.ImagesBase64);
                    var target = effectInterface.hajiMaker(srcimg, Convert.ToInt32(photo.Text ?? "1"), (int)(photo.Level ?? 0.0f));
                    return new PhotoResponse() { Type = ResponseTypes.Ok, ImageBase64 = BitmapConvert.ToBase64(target) };

                }
                return new PhotoResponse() { Type = ResponseTypes.FailUnknownEffect };
            }
            catch (System.Exception e)
            {
                String FilePathLog = HttpContext.Current.Server.MapPath(@"../../data/" + "CSLog.log");
                //DateTime.Now.ToString("dd-HH-mm-ss");
                //.System.System.String data = JsonConvert.SerializeObject(photo);
                File.WriteAllText(FilePathLog, e.ToString());
                return new PhotoResponse() { Type = ResponseTypes.FailCSException,ErrorMessage=e.ToString() };

            }

        }
        public Bitmap GrayScaleFilter(Bitmap image)
        {
            Bitmap grayScale = new Bitmap(image.Width, image.Height);

            for (Int32 y = 0; y < grayScale.Height; y++)
                for (Int32 x = 0; x < grayScale.Width; x++)
                {
                    Color c = image.GetPixel(x, y);
                    Int32 gs = (Int32)(c.R * 0.3 + c.G * 0.59 + c.B * 0.11);
                    grayScale.SetPixel(x, y, Color.FromArgb(gs, gs, gs));
                }
            return grayScale;
        }
        [HttpGet]
        public PhotoResponse Test2()
        {
            Bitmap image = new Bitmap(@"f:\fff.jpg");
            //  TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
            //  Bitmap image = (Bitmap)tc.ConvertFrom(so.FileName);

            var img = GrayScaleFilter(image);
            //  MemoryStream ms = new MemoryStream();

            return new PhotoResponse() { Type = ResponseTypes.Ok, ImageBase64 = BitmapConvert.ToBase64(img) };
        }
        [HttpGet]
        public string Test3()
        {
            var s = "aaa";
            for (int i = 0; i < 20; i++)
                s += s;
            return "Ok" + s;
        }
    }
}
